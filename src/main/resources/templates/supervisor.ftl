<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>categorysalesplan</title>
</head>
<body>

<#if exception??><div> ${exception}</div> </#if>

<h1>Личный кабинет администратора ${supervisorID} </h1>

<form name="searchForm" action="/addcategory" method="GET">
    <input type="text"  name="categoryName" placeholder="categoryName"/>
    <input type="text"  name="description" placeholder="description">
    <input type="submit" value="Добавить категорию" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<form name="searchForm" action="/addgeneralplan" method="GET">
    <input type="text"  name="quarter" placeholder="quarter"/>
    <input type="text"  name="year" placeholder="year">
    <input type="submit" value="Добавить план продаж организации" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

<div>Категории</div>
<#if categories??>
    <form name="categoriesForm">
        <table id="table product" border="1px" cellspacing="1">
            <tr>
                <th>№</th><th>Категория</th><th>Описание</th>
            </tr>
            <#list categories as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>
                    <td><button formaction="/category" method="GET" value="${categories[row?index][0]}"
                                name="categoryName" type="submit"
                                style="width: 100%; height: 100%">Редактировать</button>
                </tr>
            </#list>
        </table>
    </form>
</#if>

<div>Планы продаж организаци</div>
<#if generalSalesPlans??>
    <form name="CategorySalesPlanForm">
        <table id="table product" border="1px" cellspacing="1">
            <tr>
                <th>№</th><th>Квартал</th><th>Год</th><th>Статус</th>
            </tr>
            <#list generalSalesPlans as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>
                    <td><button formaction="/generalsalesplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Редактировать</button>
                        <input type="hidden"  name="quarter${row?index}" value="${generalSalesPlans[row?index][0]}" />
                        <input type="hidden"  name="year${row?index}" value="${generalSalesPlans[row?index][1]}"  />
                    </td>
                    <td><button formaction="/acceptgeneralsalesplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Утвердить</button>
                        <input type="hidden"  name="quarter${row?index}" value="${generalSalesPlans[row?index][0]}" />
                        <input type="hidden"  name="year${row?index}" value="${generalSalesPlans[row?index][1]}"  />
                    </td>
                    <td><button formaction="/rejectgeneralsalesplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Отклонить</button>
                        <input type="hidden"  name="quarter${row?index}" value="${generalSalesPlans[row?index][0]}" />
                        <input type="hidden"  name="year${row?index}" value="${generalSalesPlans[row?index][1]}"  />
                    </td>
                </tr>
            </#list>
        </table>
    </form>
</#if>



</body>
</html>