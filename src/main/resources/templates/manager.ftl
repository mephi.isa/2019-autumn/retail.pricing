<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>categorysalesplan</title>
</head>
<body>

<#if exception??><div> ${exception}</div> </#if>

<h1>Личный кабинет менеджера ${managerID} </h1>

<div>Мои категории</div>
<#if categories??>
    <form name="categoriesForm">
        <table id="table product" border="1px" cellspacing="1">
            <tr>
                <th>№</th><th>Категория</th><th>Описание</th>
            </tr>
            <#list categories as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>
                    <td><button formaction="/category" method="GET" value="${categories[row?index][0]}"
                                name="categoryName" type="submit"
                                style="width: 100%; height: 100%">Редактировать</button>
                </tr>
            </#list>
        </table>
    </form>
</#if>

<div>Мои планы</div>
<#if categorySalesPlans??>
    <form name="CategorySalesPlanForm">
        <table id="table product" border="1px" cellspacing="1">
            <tr>
                <th>№</th><th>Категория</th><th>Квартал</th><th>Год</th><th>Статус</th>
            </tr>
            <#list categorySalesPlans as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>
                    <td><button formaction="/categorysalesplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Редактировать</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${categorySalesPlans[row?index][0]}" />
                        <input type="hidden"  name="quarter${row?index}" value="${categorySalesPlans[row?index][1]}" />
                        <input type="hidden"  name="year${row?index}" value="${categorySalesPlans[row?index][2]}"  />
                    </td>
                    <td><form>
                            <button formaction="/completecategoryplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Завершить</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${categorySalesPlans[row?index][0]}" />
                        <input type="hidden"  name="quarter${row?index}" value="${categorySalesPlans[row?index][1]}" />
                        <input type="hidden"  name="year${row?index}" value="${categorySalesPlans[row?index][2]}"  />
                        </form>
                    </td>
                    <td><form><button formaction="/acceptcategoryplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Утвердить</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${categorySalesPlans[row?index][0]}" />
                        <input type="hidden"  name="quarter${row?index}" value="${categorySalesPlans[row?index][1]}" />
                        <input type="hidden"  name="year${row?index}" value="${categorySalesPlans[row?index][2]}"  />
                        </form>
                    </td>
                    <td><form><button formaction="/rejectcategoryplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Отклонить</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${categorySalesPlans[row?index][0]}" />
                        <input type="hidden"  name="quarter${row?index}" value="${categorySalesPlans[row?index][1]}" />
                        <input type="hidden"  name="year${row?index}" value="${categorySalesPlans[row?index][2]}"  />
                        </form>
                    </td>
                </tr>
            </#list>
        </table>
    </form>
</#if>



</body>
</html>