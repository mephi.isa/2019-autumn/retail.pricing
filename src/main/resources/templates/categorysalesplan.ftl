<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>categorysalesplan</title>
</head>
<body>

<#if exception??><div> ${exception}</div> </#if>

<h1>План продаж категории ${categoryName} на ${quarter} квартал ${year} года </h1>
<#if products??>
    <form name="CategorySalesPlanForm">
    <input type="hidden"  name="categoryName" value="${categoryName}"  />
    <input type="hidden"  name="year" value="${year}"  />
    <input type="hidden"  name="quarter" value="${quarter}" />

    <table id="table product" border="1px" cellspacing="1">
        <tr>
            <th>№</th><th>Артикул</th><th>Наименование</th><th>Количество</th><th>Цена</th>
        </tr>
        <#list products as row>
            <tr>
                <td>${row?index + 1}</td>
                <#list row as field>
                    <td>${field}</td>
                </#list>
                <td><button formaction="/setproductquantity" method="GET" value="${products[row?index][0]}"
                            name="productArticle" type="submit" title="${products[row?index][1]}"
                            style="width: 100%; height: 100%">Изменить количество</button></td>
                <td><input type="text" name="quantity${products[row?index][0]}" placeholder="new quantity"></td>
            </tr>
        </#list>
    </table>
</form>
</#if>

</body>
</html>