<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>categorysalesplan</title>
</head>
<body>

<#if exception??><div> ${exception}</div> </#if>

<h1>Личный кабинет аналитика ${analystID} </h1>

<div>Планы для заполнения</div>
<#if categorySalesPlans??>
    <form name="CategorySalesPlanForm">
        <table id="table product" border="1px" cellspacing="1">
            <tr>
                <th>№</th><th>Категория</th><th>Квартал</th><th>Год</th><th>Статус</th>
            </tr>
            <#list categorySalesPlans as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>
                    <td><button formaction="/categorysalesplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Редактировать</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${categorySalesPlans[row?index][0]}" />
                        <input type="hidden"  name="quarter${row?index}" value="${categorySalesPlans[row?index][1]}" />
                        <input type="hidden"  name="year${row?index}" value="${categorySalesPlans[row?index][2]}"  />
                    </td>
                    <td><button formaction="/completecategoryplan" method="GET" value="${row?index}"
                                name="index" type="submit"
                                style="width: 100%; height: 100%">Отправить</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${categorySalesPlans[row?index][0]}" />
                        <input type="hidden"  name="quarter${row?index}" value="${categorySalesPlans[row?index][1]}" />
                        <input type="hidden"  name="year${row?index}" value="${categorySalesPlans[row?index][2]}"  />
                    </td>
                </tr>
            </#list>
        </table>
    </form>
<#else>
    <div>Планов для заполнения нет</div>
</#if>

</body>
</html>