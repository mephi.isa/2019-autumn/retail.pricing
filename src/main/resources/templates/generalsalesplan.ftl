<#import "ui.ftl" as ui/>

<!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>generalsalesplan</title>
</head>
<body>

<h1>Общий план продаж на ${quarter} квартал ${year} года </h1>

<#if plans??>
    <form name="GeneralSalesPlan" >
        <input type="hidden"  name="year" value="${year}"  />
        <input type="hidden"  name="quarter" value="${quarter}" />

        <table id="table product" border="1px" cellspacing="1">
            <tr>
                <th>№</th><th>Категория</th><th>Статус</th>
            </tr>
            <#list plans as row>
                <tr>
                    <td>${row?index + 1}</td>
                    <#list row as field>
                        <td>${field}</td>
                    </#list>
                    <td><button formaction="/categorysalesplan" method="GET" value="${row?index}"
                                name="index" type="submit" title="${plans[row?index][0]}"
                                style="width: 100%; height: 100%">Смотреть</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${plans[row?index][0]}"  />
                        <input type="hidden"  name="year${row?index}" value="${year}"  />
                        <input type="hidden"  name="quarter${row?index}" value="${quarter}" />
                    </td>

                    <td><button formaction="/acceptcategoryplan" method="GET" value="${row?index}"
                                name="index" type="submit" title="${plans[row?index][0]}"
                                style="width: 100%; height: 100%">Утвердить</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${plans[row?index][0]}"  />
                        <input type="hidden"  name="year${row?index}" value="${year}"  />
                        <input type="hidden"  name="quarter${row?index}" value="${quarter}" />
                    </td>

                    <td><button formaction="/rejectcategoryplan" method="GET" value="${row?index}"
                                name="index" type="submit" title="${plans[row?index][0]}"
                                style="width: 100%; height: 100%">Отклонить</button>
                        <input type="hidden"  name="categoryName${row?index}" value="${plans[row?index][0]}"  />
                        <input type="hidden"  name="year${row?index}" value="${year}"  />
                        <input type="hidden"  name="quarter${row?index}" value="${quarter}" />
                    </td>
                </tr>
            </#list>
        </table>
    </form>
</#if>

<#if exception??><div>${exception}</div> </#if>

</body>
</html>