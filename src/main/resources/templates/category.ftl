<#import "ui.ftl" as ui/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>category</title>
</head>
<body>

<#if categoryName??>
    <h1>${categoryName}</h1>
<#if user??> <#if user == "SUPERVISOR">

    <form name="setmanagerForm" action="/setmanager" method="GET">
        <input type="hidden"  name="categoryName" value="${categoryName}"  />
        <label>
            <select name="managerID">
                <#list availableManagers as it>
                    <#list it as tmplattribute>
                        <option name ="managerID" value="${tmplattribute}">${tmplattribute}</option>
                    </#list>
                </#list>
                <input type="submit" value="Выбрать менеджера" />
            </select>
        </label>
        <#if add_product_status??><div>${add_product_status}</div></#if>
    </form>

</#if></#if>

<#if manager??>
<div> Менеджер категории " ${manager} " </div>
</#if>

<#if user??> <#if user == "MANAGER" || user == "SUPERVISOR">

<form name="searchForm" action="/addproduct" method="GET">
    <input type="hidden"  name="categoryName" value="${categoryName}"  />
    <input type="text" name="productArticle" placeholder="productArticle">
    <input type="text" name="productName" placeholder="productName">
    <input type="text" name="description" placeholder="description">
    <input type="submit" value="Добавить продукт" />
    <#if add_product_status??><div>${add_product_status}</div></#if>
</form>

</#if></#if>



<#if products??>
    <div> Продукты в категории </div>
<form>
    <table id="table product" border="1px" cellspacing="1">
        <tr>
            <th>№</th><th>Артикул</th><th>Название</th>
        </tr>
        <#list products as row>
            <tr>
                <td>${row?index + 1}</td>
                <#list row as field>
                    <td>${field}</td>
                </#list>
            </tr>
        </#list>
    </table>
</form>
</#if>

<#if user??> <#if user == "MANAGER" || user == "SUPERVISOR">
<form name="addanalystForm" action="/addanalyst" method="GET">
    <input type="hidden"  name="categoryName" value="${categoryName}"  />
    <label>
        <select name="analystID">
            <#list availableAnalysts as it>
                <#list it as tmplattribute>
                    <option name ="analystID" value="${tmplattribute}">${tmplattribute}</option>
                </#list>
            </#list>
            <input type="submit" value="Добавить аналитика" />
        </select>
    </label>
    <#if add_analyst_status??><div>${add_analyst_status}</div></#if>
</form>

</#if></#if>



    <#if analysts??>
<div> Список аналитиков в категории </div>
<form>
    <input type="hidden"  name="categoryName" value="${categoryName}"  />
    <table id="table analysts" border="1px" cellspacing="1" border="1">
        <tr>
            <th>№</th><th>personal ID</th>
        </tr>
        <#list analysts as row>
            <tr>
            <td>${row?index + 1}</td>
            <#list row as field>
                <td>${field}</td>
            </#list>
                <td>
                <#if user??> <#if user == "SUPERVISOR" || user == "MANAGER" >
                    <button formaction="/remanalyst" name="analystID" type="submit"
                            value="${analysts[row?index][0]}" style="width: 100%; height: 100%">Удалить</button>
                </#if></#if>
                </td>
            </tr>
        </#list>
    </table>
</form>
</#if>
</#if>

<#if exception??><div>${exception}</div> </#if>

</body>
</html>
