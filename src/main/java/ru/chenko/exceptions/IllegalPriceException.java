package ru.chenko.exceptions;

public class IllegalPriceException extends Exception {
    public IllegalPriceException(String message) {
        super(message);
    }
}
