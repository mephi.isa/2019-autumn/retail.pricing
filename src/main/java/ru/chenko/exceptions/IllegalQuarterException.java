package ru.chenko.exceptions;

public class IllegalQuarterException extends Exception{
    public IllegalQuarterException(String message) {
        super(message);
    }
}
