package ru.chenko.exceptions;

public class IllegalCountException extends Exception {
    public IllegalCountException(String message) {
        super(message);
    }
}
