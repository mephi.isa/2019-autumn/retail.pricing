package ru.chenko.exceptions;

public class IllegalYearException extends Exception {
    public IllegalYearException(String message) {
        super(message);
    }
}
