package ru.chenko.exceptions;

public class IllegalSalesPlanStateException extends Exception{
    public IllegalSalesPlanStateException(String message) {
        super(message);
    }
}
