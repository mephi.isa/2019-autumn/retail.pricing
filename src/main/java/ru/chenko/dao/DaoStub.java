package ru.chenko.dao;


import ru.chenko.models.*;

import java.util.*;
import java.util.List;

public class DaoStub implements Dao {

    private Map<String, Product> ProductMap = new HashMap<>();
    private Map<String, Category> CategoryMap = new HashMap<>();
    private List<CategorySalesPlan> CategorySalesPlanList = new ArrayList<>();
    private Map<Quarter, GeneralSalesPlan> GeneralSalesPlanMap = new HashMap<>();

    public DaoStub() {
    }

    @Override
    public List<Category> getCategoryList() {
        return new ArrayList<>(this.CategoryMap.values());
    }

    @Override
    public Category getCategoryByName(String name){ return this.CategoryMap.get(name); }

    @Override
    public void addCategory(Category category){
        this.CategoryMap.put(category.getName(), category);
    }

    @Override
    public void updateCategory(Category category){ this.CategoryMap.put(category.getName(), category); }

    @Override
    public CategorySalesPlan getCategorySalesPlan(String CategoryName,Quarter quarter){
        for (CategorySalesPlan categorySalesPlan : CategorySalesPlanList) {
            if ((categorySalesPlan.getQuarter().getYear().equals(quarter.getYear())) &&
                    (categorySalesPlan.getQuarter().getQuarter().equals(quarter.getQuarter()))&&
                    (categorySalesPlan.getCategory().equals(CategoryName))) {
                return categorySalesPlan;
            }
        }
        return null;
    }

    @Override
    public List<CategorySalesPlan> getCategorySalesPlanList(){
        return this.CategorySalesPlanList; }

    @Override
    public void updateCategorySalesPlan(CategorySalesPlan categorySalesPlan){
        for(int i = 0; i < CategorySalesPlanList.size();i++){
            if((CategorySalesPlanList.get(i).getCategory().equals(categorySalesPlan.getCategory()))&&
                    (CategorySalesPlanList.get(i).getQuarter().getYear() == categorySalesPlan.getQuarter().getYear())&&
                    (CategorySalesPlanList.get(i).getQuarter().getQuarter() == categorySalesPlan.getQuarter().getQuarter())
            ){
                CategorySalesPlanList.remove(i);
                CategorySalesPlanList.add(categorySalesPlan);
                break;
            }
        }
    }

    @Override
    public Product getProductByArticle(String productArticle){
        return this.ProductMap.get(productArticle);
    }

    @Override
    public List<Product> getProductList(){
        List<Product> productList = new ArrayList<>(ProductMap.values());
        return productList;
    }

    @Override
    public void addProduct(Product product){
        this.ProductMap.put(product.getArticle(),product);
    }

    @Override
    public void updateProduct(Product product){
        this.ProductMap.put(product.getArticle(),product);
    }

    @Override
    public void addCategorySalesPlan(CategorySalesPlan categorySalesPlan){
        this.CategorySalesPlanList.add(categorySalesPlan);
    }

    @Override
    public List<GeneralSalesPlan> getGeneralSalesPlanList(){
        return new ArrayList<>(this.GeneralSalesPlanMap.values());
    }

    @Override
    public GeneralSalesPlan getGeneralSalesPlanByQuarter(Quarter quarter){
        return this.GeneralSalesPlanMap.get(quarter);
    }

    @Override
    public void addGeneralSalesPlan(GeneralSalesPlan generalSalesPlan){
        this.GeneralSalesPlanMap.put(generalSalesPlan.getQuarter(), generalSalesPlan);
    }

    @Override
    public void updateGeneralSalesPlan(GeneralSalesPlan generalSalesPlan){

    }

}
