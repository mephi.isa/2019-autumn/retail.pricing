/*Стыдно за всю эту фигню ниже*/

package ru.chenko.dao;

import ru.chenko.Dto.CategoryDto;
import ru.chenko.Dto.CategorySalesPlanDto;
import ru.chenko.Dto.GeneralSalesPlanDto;
import ru.chenko.Dto.ProductDto;
import ru.chenko.exceptions.IllegalCountException;
import ru.chenko.exceptions.IllegalQuarterException;
import ru.chenko.exceptions.IllegalYearException;
import ru.chenko.models.*;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DaoPostgresV2 implements Dao {
    private int BufSize = 4096;
    private String sql;
    private Connection c;
    private PreparedStatement stmt;

    public DaoPostgresV2() {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return;
        }

        try {
            c = DriverManager
                    .getConnection("jdbc:postgresql://127.0.0.1:5432/retail_prising_db_v2", "ais_user", "ais654321");
            c.setAutoCommit(false);
            System.out.println("-- Opened database successfully");
        } catch (Exception ex) {
            System.out.println("Connection failed...");
            ex.printStackTrace();
        }


    }

    @Override
    public List<Category> getCategoryList() throws SQLException, IOException, ClassNotFoundException {
        List<Category> categoryList = new ArrayList<>();

        sql = "SELECT * FROM categories;";
        stmt = c.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.name = rs.getString("name");
            categoryDto.description = rs.getString("description");
            categoryDto.manager = rs.getLong("manager");
            categoryDto.analystList = Arrays.asList((Long[])rs.getArray("analystlist").getArray());
            categoryDto.productArticlesList = Arrays.asList((String[])rs.getArray("productarticleslist").getArray());
            Category category = new Category(categoryDto);
            categoryList.add(category);
        }

        return categoryList;
    }

    @Override
    public Category getCategoryByName(String categoryName) throws SQLException, IOException, ClassNotFoundException {

        sql = "SELECT * FROM categories WHERE name=?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,categoryName);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.name = rs.getString("name");
        categoryDto.description = rs.getString("description");
        categoryDto.manager = rs.getLong("manager");
        categoryDto.analystList = Arrays.asList((Long[])rs.getArray("analystlist").getArray());
        categoryDto.productArticlesList = Arrays.asList((String[])rs.getArray("productarticleslist").getArray());
        return new Category(categoryDto);

    }

    @Override
    public void addCategory(Category category) throws IOException, SQLException {
        category.getAnalystSet().toArray();
        Long[] a = new Long[10];

        sql = "INSERT INTO categories (name,description,manager,analystlist,productarticleslist) VALUES (?,?,?,?,?);";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,category.getName());
        stmt.setString(2,category.getDescription());
        stmt.setLong(3,category.getManager());
        stmt.setArray(4, c.createArrayOf("bigint",category.getAnalystSet().toArray()));
        stmt.setArray(5, c.createArrayOf("text",category.getProductSet().toArray()));
        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Insert failed...");
            ex.printStackTrace();
        }
        stmt.close();
        c.commit();
    }

    @Override
    public void updateCategory(Category category) throws IOException, SQLException {

        sql = "UPDATE categories set description = ?,manager = ?, analystlist = ?, productarticleslist = ? where name=?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(5,category.getName());
        stmt.setString(1,category.getDescription());
        stmt.setLong(2,category.getManager());
        stmt.setArray(3, c.createArrayOf("bigint",category.getAnalystSet().toArray()));
        stmt.setArray(4, c.createArrayOf("text",category.getProductSet().toArray()));

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Update failed...");
            ex.printStackTrace();
        }
        stmt.close();
        c.commit();
    }


    @Override
    public Product getProductByArticle(String productArticle) throws IOException, SQLException, ClassNotFoundException {

        sql = "SELECT * FROM products WHERE article=?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,productArticle);
        ProductDto productDto = new ProductDto();
        try {
            ResultSet rs = stmt.executeQuery();
            rs.next();
            productDto.name = rs.getString("name");
            productDto.categoryName = rs.getString("categoryName");
            productDto.description = rs.getString("description");
            productDto.article = rs.getString("article");
        }
        catch(Exception ex) {
            System.out.println("Get Product failed...");
            ex.printStackTrace();
        }
        return new Product(productDto);
    }

    @Override
    public List<Product> getProductList() throws SQLException, IOException, ClassNotFoundException {
        List<Product> productList = new ArrayList<>();

        sql = "SELECT * FROM products;";
        stmt = c.prepareStatement(sql);
        ProductDto productDto = new ProductDto();
        try {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                productDto.name = rs.getString("name");
                productDto.categoryName = rs.getString("categoryName");
                productDto.description = rs.getString("description");
                productDto.article = rs.getString("article");
                productList.add(new Product(productDto));
            }
        }
        catch(Exception ex) {
            System.out.println("Get Product List failed...");
            ex.printStackTrace();
        }
        return productList;
    }

    @Override
    public void addProduct(Product product) throws SQLException {

        sql = "INSERT INTO products (article,name,categoryName) VALUES (?,?,?);";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,product.getArticle());
        stmt.setString(2,product.getName());
        stmt.setString(3,product.getCategoryName());

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Insert failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
    }

    @Override
    public void updateProduct(Product product) throws SQLException {

        sql = "UPDATE products set name = ?, categoryName = ? where article=?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(3,product.getArticle());
        stmt.setString(1,product.getName());
        stmt.setString(2,product.getCategoryName());

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Update failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
    }


    @Override
    public CategorySalesPlan getCategorySalesPlan(String categoryName, Quarter quarter) throws IOException, ClassNotFoundException, SQLException, IllegalQuarterException, IllegalYearException, IllegalCountException {

        List<String> products;
        List<Integer> quantity;
        CategorySalesPlanDto categorySalesPlanDto =new CategorySalesPlanDto();
        sql = "SELECT * FROM CategorySalesPlans WHERE category=? AND quarter =? AND year =?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,categoryName);
        stmt.setInt(2,quarter.getQuarter());
        stmt.setInt(3,quarter.getYear());
        ResultSet rs = stmt.executeQuery();
        rs.next();
        categorySalesPlanDto.categoryName = rs.getString("category");
        categorySalesPlanDto.quarter = new Quarter(rs.getInt("quarter"),rs.getInt("year"));
        switch (rs.getString("status")){
            case "CREATED":
                categorySalesPlanDto.status = Status.CREATED;
                break;
            case "COMPLETED":
                categorySalesPlanDto.status = Status.COMPLETED;
                break;
            case "ACCEPTED":
                categorySalesPlanDto.status = Status.ACCEPTED;
                break;
            case "REJECTED":
                categorySalesPlanDto.status = Status.REJECTED;
                break;
            default:
                categorySalesPlanDto.status = Status.CREATED;
                break;
        }
        products = Arrays.asList((String[])rs.getArray("products").getArray());
        quantity = Arrays.asList((Integer[]) rs.getArray("quantity").getArray());

        for(int i = 0; i < products.size();i++){
            SalesPlanRow salesPlanRow = new SalesPlanRow(products.get(i));
            salesPlanRow.setCount(quantity.get(i));
            categorySalesPlanDto.salesPlanRowList.add(salesPlanRow);
        }
        return new CategorySalesPlan(categorySalesPlanDto);
    }

    @Override
    public List<CategorySalesPlan> getCategorySalesPlanList() throws SQLException, IOException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        List<CategorySalesPlan> categoryList = new ArrayList<>();
        List<String> products;
        List<Integer> quantity;

        sql = "SELECT * FROM CategorySalesPlans;";
        stmt = c.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            CategorySalesPlanDto categorySalesPlanDto =new CategorySalesPlanDto();
            categorySalesPlanDto.categoryName = rs.getString("category");
            categorySalesPlanDto.quarter = new Quarter(rs.getInt("quarter"),rs.getInt("year"));
            switch (rs.getString("status")){
                case "CREATED":
                    categorySalesPlanDto.status = Status.CREATED;
                    break;
                case "COMPLETED":
                    categorySalesPlanDto.status = Status.COMPLETED;
                    break;
                case "ACCEPTED":
                    categorySalesPlanDto.status = Status.ACCEPTED;
                    break;
                case "REJECTED":
                    categorySalesPlanDto.status = Status.REJECTED;
                    break;
                default:
                    categorySalesPlanDto.status = Status.CREATED;
                    break;
            }
            products = Arrays.asList((String[])rs.getArray("products").getArray());
            quantity = Arrays.asList((Integer[]) rs.getArray("quantity").getArray());

            for(int i = 0; i < products.size();i++){
                SalesPlanRow salesPlanRow = new SalesPlanRow(products.get(i));
                salesPlanRow.setCount(quantity.get(i));
                categorySalesPlanDto.salesPlanRowList.add(salesPlanRow);
            }
            categoryList.add(new CategorySalesPlan(categorySalesPlanDto));
        }

        return categoryList;
    }

    @Override
    public void updateCategorySalesPlan(CategorySalesPlan categorySalesPlan) throws  SQLException {
        List<String> products = new ArrayList<>();
        List<Integer> quantity = new ArrayList<>();
        List<SalesPlanRow> salesPlanRowList = new ArrayList<>(categorySalesPlan.getSalesPlanRowSet().values());

        for(int i = 0; i < salesPlanRowList.size();i++){
            products.add(salesPlanRowList.get(i).getProductArticle());
            quantity.add(salesPlanRowList.get(i).getCount());
        }

        sql = "UPDATE CategorySalesPlans set status = ?, products = ?, quantity = ? WHERE category=? AND quarter = ? AND year = ?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,categorySalesPlan.getStatus().toString());
        stmt.setArray(2,
                c.createArrayOf("text",products.toArray()));
        stmt.setArray(3,
                c.createArrayOf("integer",quantity.toArray()));
        stmt.setString(4,categorySalesPlan.getCategory());
        stmt.setInt(5,categorySalesPlan.getQuarter().getQuarter());
        stmt.setInt(6,categorySalesPlan.getQuarter().getYear());
        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Update failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
    }

    @Override
    public void addCategorySalesPlan(CategorySalesPlan categorySalesPlan) throws IOException, SQLException {
        List<String> products = new ArrayList<>();
        List<Integer> quantity = new ArrayList<>();

        for(int i = 0; i < categorySalesPlan.getSalesPlanRowSet().size(); i++){
            products.add(categorySalesPlan.getSalesPlanRowSet().values().iterator().next().getProductArticle());
            quantity.add(categorySalesPlan.getSalesPlanRowSet().values().iterator().next().getCount());
        }

        sql = "INSERT INTO CategorySalesPlans (category,quarter,year,status,products,quantity) VALUES (?,?,?,?,?,?);";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,categorySalesPlan.getCategory());
        stmt.setInt(2,categorySalesPlan.getQuarter().getQuarter());
        stmt.setInt(3,categorySalesPlan.getQuarter().getYear());
        stmt.setString(4,categorySalesPlan.getStatus().toString());
        stmt.setArray(5,
                c.createArrayOf("text",products.toArray()));
        stmt.setArray(6,
                c.createArrayOf("integer",quantity.toArray()));
        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Insert failed...");
            ex.printStackTrace();
        }
        stmt.close();
        c.commit();
    }


    @Override
    public List<GeneralSalesPlan> getGeneralSalesPlanList() throws SQLException {
        List<GeneralSalesPlan> generalSalesPlanList = new ArrayList<>();

        sql = "SELECT * FROM GeneralSalesPlans;";
        stmt = c.prepareStatement(sql);
        GeneralSalesPlanDto generalSalesPlanDto = new GeneralSalesPlanDto();
        try {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                generalSalesPlanDto.quarter =
                        new Quarter(rs.getInt("quarter"),rs.getInt("year"));
                generalSalesPlanDto.categorySalesPlanList =
                        Arrays.asList((String[])rs.getArray("categorySalesPlanList").getArray());
                switch (rs.getString("status")){
                    case "CREATED":
                        generalSalesPlanDto.status = Status.CREATED;
                        break;
                    case "COMPLETED":
                        generalSalesPlanDto.status = Status.COMPLETED;
                        break;
                    case "ACCEPTED":
                        generalSalesPlanDto.status = Status.ACCEPTED;
                        break;
                    case "REJECTED":
                        generalSalesPlanDto.status = Status.REJECTED;
                        break;
                    default:
                        generalSalesPlanDto.status = Status.CREATED;
                        break;
                }
                generalSalesPlanList.add(new GeneralSalesPlan(generalSalesPlanDto));
            }
        }
        catch(Exception ex) {
            System.out.println("Get General Sales Plan List failed...");
            ex.printStackTrace();
        }
        return generalSalesPlanList;
    }

    @Override
    public GeneralSalesPlan getGeneralSalesPlanByQuarter(Quarter quarter) throws SQLException, IllegalQuarterException, IllegalYearException {
        sql = "SELECT * FROM GeneralSalesPlans WHERE quarter = ? AND year = ?;";
        stmt = c.prepareStatement(sql);
        stmt.setInt(1,quarter.getQuarter());
        stmt.setInt(2,quarter.getYear());
        ResultSet rs = stmt.executeQuery();
        GeneralSalesPlanDto generalSalesPlanDto = new GeneralSalesPlanDto();
        rs.next();
        generalSalesPlanDto.quarter =
                new Quarter(rs.getInt("quarter"),rs.getInt("year"));
        try {
            generalSalesPlanDto.categorySalesPlanList =
                    Arrays.asList((String[]) rs.getArray("categorysalesplanlist").getArray());
        }catch (Exception ignored){

        }
        switch (rs.getString("status")){
            case "CREATED":
                generalSalesPlanDto.status = Status.CREATED;
                break;
            case "COMPLETED":
                generalSalesPlanDto.status = Status.COMPLETED;
                break;
            case "ACCEPTED":
                generalSalesPlanDto.status = Status.ACCEPTED;
                break;
            case "REJECTED":
                generalSalesPlanDto.status = Status.REJECTED;
                break;
            default:
                generalSalesPlanDto.status = Status.CREATED;
                break;
        }

        return new GeneralSalesPlan(generalSalesPlanDto);
    }

    @Override
    public void addGeneralSalesPlan(GeneralSalesPlan generalSalesPlan) throws SQLException {
        sql = "INSERT INTO GeneralSalesPlans (quarter,year,status,categorysalesplanlist) VALUES (?,?,?,?);";
        stmt = c.prepareStatement(sql);
        stmt.setInt(1,generalSalesPlan.getQuarter().getQuarter());
        stmt.setInt(2,generalSalesPlan.getQuarter().getYear());
        stmt.setString(3,generalSalesPlan.getStatus().toString());
        stmt.setArray(4,c.createArrayOf("text",generalSalesPlan.getCategorySalesPlanSet().toArray()));

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Insert failed...");
            ex.printStackTrace();
        }
        stmt.close();
        c.commit();
    }

    @Override
    public void updateGeneralSalesPlan(GeneralSalesPlan generalSalesPlan) throws SQLException {

        sql = "UPDATE GeneralSalesPlans set status = ?, categorysalesplanlist  = ? WHERE quarter = ? AND year = ?;";
        stmt = c.prepareStatement(sql);
        stmt.setInt(3,generalSalesPlan.getQuarter().getQuarter());
        stmt.setInt(4,generalSalesPlan.getQuarter().getYear());
        stmt.setString(1,generalSalesPlan.getStatus().toString());
        stmt.setArray(2,c.createArrayOf("text",generalSalesPlan.getCategorySalesPlanSet().toArray()));
        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Update failed...");
            ex.printStackTrace();
        }
        stmt.close();
        c.commit();
    }


}


