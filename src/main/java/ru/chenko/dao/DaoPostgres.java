package ru.chenko.dao;

import ru.chenko.models.*;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoPostgres implements Dao {
    private int BufSize = 4096;
    private String sql;
    private Connection c;
    private PreparedStatement stmt;

    public DaoPostgres() {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return;
        }

        try {
            c = DriverManager
                    .getConnection("jdbc:postgresql://127.0.0.1:5432/retail_prising_db", "ais_user", "ais654321");
            c.setAutoCommit(false);
            System.out.println("-- Opened database successfully");
        } catch (Exception ex) {
            System.out.println("Connection failed...");
            ex.printStackTrace();
        }


    }

    @Override
    public List<Category> getCategoryList() throws SQLException, IOException, ClassNotFoundException {
        List<Category> categoryList = new ArrayList<>();

        sql = "SELECT * FROM categories;";
        stmt = c.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            ByteArrayInputStream bos = new ByteArrayInputStream(rs.getBytes("object"));
            ObjectInputStream objectInputStream = new ObjectInputStream(bos);
            categoryList.add((Category) objectInputStream.readObject());
        }

        return categoryList;
    }

    @Override
    public Category getCategoryByName(String categoryName) throws SQLException, IOException, ClassNotFoundException {

        sql = "SELECT * FROM categories WHERE name=?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,categoryName);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        ByteArrayInputStream bos = new ByteArrayInputStream(rs.getBytes("object"));
        ObjectInputStream objectInputStream = new ObjectInputStream(bos);
        return (Category) objectInputStream.readObject();

    }

    @Override
    public void addCategory(Category category) throws IOException, SQLException {

        ByteArrayOutputStream bos;
        bos = new ByteArrayOutputStream(BufSize);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(category);

        ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());

        sql = "INSERT INTO categories (name,object) VALUES (?,?);";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,category.getName());
        stmt.setBinaryStream(2,bais);

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Insert failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
        objectOutputStream.close();
    }

    @Override
    public void updateCategory(Category category) throws IOException, SQLException {

        ByteArrayOutputStream bos;
        bos = new ByteArrayOutputStream(BufSize);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(category);

        ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());

        sql = "UPDATE categories set OBJECT = ? where name=?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(2,category.getName());
        stmt.setBinaryStream(1,bais);

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Update failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
        objectOutputStream.close();
    }


    @Override
    public Product getProductByArticle(String productArticle) throws IOException, SQLException, ClassNotFoundException {

        sql = "SELECT * FROM products WHERE article=?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,productArticle);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        ByteArrayInputStream bos = new ByteArrayInputStream(rs.getBytes("object"));
        ObjectInputStream objectInputStream = new ObjectInputStream(bos);
        return (Product) objectInputStream.readObject();


    }

    @Override
    public List<Product> getProductList() throws SQLException, IOException, ClassNotFoundException {
        List<Product> productList = new ArrayList<>();

        sql = "SELECT * FROM products;";
        stmt = c.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            ByteArrayInputStream bos = new ByteArrayInputStream(rs.getBytes("object"));
            ObjectInputStream objectInputStream = new ObjectInputStream(bos);
            productList.add((Product) objectInputStream.readObject());
        }
        return productList;
    }

    @Override
    public void addProduct(Product product) throws IOException, SQLException {

        ByteArrayOutputStream bos;
        bos = new ByteArrayOutputStream(BufSize);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(product);

        ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());

        sql = "INSERT INTO products (article,object) VALUES (?,?);";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,product.getArticle());
        stmt.setBinaryStream(2,bais);

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Insert failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
        objectOutputStream.close();


    }

    @Override
    public void updateProduct(Product product) throws IOException, SQLException {

        ByteArrayOutputStream bos;
        bos = new ByteArrayOutputStream(BufSize);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(product);

        ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());

        sql = "UPDATE products set OBJECT = ? where article=?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(2,product.getArticle());
        stmt.setBinaryStream(1,bais);

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Update failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
        objectOutputStream.close();
    }


    @Override
    public CategorySalesPlan getCategorySalesPlan(String categoryName, Quarter quarter) throws IOException, ClassNotFoundException, SQLException {

        sql = "SELECT * FROM CategorySalesPlans WHERE category=? AND quarter =? AND year =?;";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,categoryName);
        stmt.setInt(2,quarter.getQuarter());
        stmt.setInt(3,quarter.getYear());
        ResultSet rs = stmt.executeQuery();
        rs.next();
        ByteArrayInputStream bos = new ByteArrayInputStream(rs.getBytes("object"));
        ObjectInputStream objectInputStream = new ObjectInputStream(bos);
        return (CategorySalesPlan) objectInputStream.readObject();
    }

    @Override
    public List<CategorySalesPlan> getCategorySalesPlanList() throws SQLException, IOException, ClassNotFoundException {
        List<CategorySalesPlan> categoryList = new ArrayList<>();

        sql = "SELECT * FROM CategorySalesPlans;";
        stmt = c.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            ByteArrayInputStream bos = new ByteArrayInputStream(rs.getBytes("object"));
            ObjectInputStream objectInputStream = new ObjectInputStream(bos);
            categoryList.add((CategorySalesPlan) objectInputStream.readObject());
        }

        return categoryList;
    }

    @Override
    public void updateCategorySalesPlan(CategorySalesPlan categorySalesPlan) throws IOException, SQLException {
        ByteArrayOutputStream bos;
        bos = new ByteArrayOutputStream(BufSize);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(categorySalesPlan);

        ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());

        sql = "UPDATE CategorySalesPlans set OBJECT = ? WHERE category=? AND quarter = ? AND year = ?;";
        stmt = c.prepareStatement(sql);
        stmt.setBinaryStream(1,bais);
        stmt.setString(2,categorySalesPlan.getCategory());
        stmt.setInt(3,categorySalesPlan.getQuarter().getQuarter());
        stmt.setInt(4,categorySalesPlan.getQuarter().getYear());

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Update failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
        objectOutputStream.close();
    }

    @Override
    public void addCategorySalesPlan(CategorySalesPlan categorySalesPlan) throws IOException, SQLException {
        ByteArrayOutputStream bos;
        bos = new ByteArrayOutputStream(BufSize);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(categorySalesPlan);

        ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());

        sql = "INSERT INTO CategorySalesPlans (category,quarter,year,object) VALUES (?,?,?,?);";
        stmt = c.prepareStatement(sql);
        stmt.setString(1,categorySalesPlan.getCategory());
        stmt.setInt(2,categorySalesPlan.getQuarter().getQuarter());
        stmt.setInt(3,categorySalesPlan.getQuarter().getYear());
        stmt.setBinaryStream(4,bais);

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Insert failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
        objectOutputStream.close();
    }


    @Override
    public List<GeneralSalesPlan> getGeneralSalesPlanList() throws SQLException, IOException, ClassNotFoundException {
        List<GeneralSalesPlan> categoryList = new ArrayList<>();

        sql = "SELECT * FROM GeneralSalesPlans;";
        stmt = c.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            ByteArrayInputStream bos = new ByteArrayInputStream(rs.getBytes("object"));
            ObjectInputStream objectInputStream = new ObjectInputStream(bos);
            categoryList.add((GeneralSalesPlan) objectInputStream.readObject());
        }

        return categoryList;
    }

    @Override
    public GeneralSalesPlan getGeneralSalesPlanByQuarter(Quarter quarter) throws SQLException, IOException, ClassNotFoundException {
        sql = "SELECT * FROM GeneralSalesPlans WHERE quarter = ? AND year = ?;";
        stmt = c.prepareStatement(sql);
        stmt.setInt(1,quarter.getQuarter());
        stmt.setInt(2,quarter.getYear());
        ResultSet rs = stmt.executeQuery();
        rs.next();
        ByteArrayInputStream bos = new ByteArrayInputStream(rs.getBytes("object"));
        ObjectInputStream objectInputStream = new ObjectInputStream(bos);
        return (GeneralSalesPlan) objectInputStream.readObject();
    }

    @Override
    public void addGeneralSalesPlan(GeneralSalesPlan generalSalesPlan) throws IOException, SQLException {

        ByteArrayOutputStream bos;
        bos = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(generalSalesPlan);

        ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());

        sql = "INSERT INTO GeneralSalesPlans (quarter,year,object) VALUES (?,?,?);";
        stmt = c.prepareStatement(sql);
        stmt.setInt(1,generalSalesPlan.getQuarter().getQuarter());
        stmt.setInt(2,generalSalesPlan.getQuarter().getYear());
        stmt.setBinaryStream(3,bais);

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Insert failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
        objectOutputStream.close();

    }

    @Override
    public void updateGeneralSalesPlan(GeneralSalesPlan generalSalesPlan) throws IOException, SQLException {

        ByteArrayOutputStream bos;
        bos = new ByteArrayOutputStream(BufSize);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(generalSalesPlan);

        ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());

        sql = "UPDATE GeneralSalesPlans set OBJECT = ? WHERE quarter = ? AND year = ?;";
        stmt = c.prepareStatement(sql);
        stmt.setBinaryStream(1,bais);
        stmt.setInt(2,generalSalesPlan.getQuarter().getQuarter());
        stmt.setInt(3,generalSalesPlan.getQuarter().getYear());

        try {
            stmt.executeUpdate();
        }
        catch(Exception ex) {
            System.out.println("Update failed...");
            ex.printStackTrace();
        }

        stmt.close();
        c.commit();
        objectOutputStream.close();
    }


}
