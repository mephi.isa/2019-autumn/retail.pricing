package ru.chenko.dao;

import ru.chenko.exceptions.IllegalCountException;
import ru.chenko.exceptions.IllegalQuarterException;
import ru.chenko.exceptions.IllegalYearException;
import ru.chenko.models.Category;
import ru.chenko.models.CategorySalesPlan;
import ru.chenko.models.GeneralSalesPlan;
import ru.chenko.models.Product;
import ru.chenko.models.Quarter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public interface Dao {

    List<Category> getCategoryList() throws SQLException, IOException, ClassNotFoundException;
    Category getCategoryByName(String name) throws SQLException, IOException, ClassNotFoundException;
    void addCategory(Category category) throws IOException, SQLException;
    void updateCategory(Category category) throws IOException, SQLException;

    Product getProductByArticle(String productArticle) throws IOException, SQLException, ClassNotFoundException;
    List<Product> getProductList() throws SQLException, IOException, ClassNotFoundException;
    void addProduct(Product product) throws IOException, SQLException;
    void updateProduct(Product product) throws IOException, SQLException;

    CategorySalesPlan getCategorySalesPlan(String CategoryName,Quarter quarter) throws IOException, ClassNotFoundException, SQLException, IllegalQuarterException, IllegalYearException, IllegalCountException;
    List<CategorySalesPlan> getCategorySalesPlanList() throws SQLException, IOException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException;
    void updateCategorySalesPlan(CategorySalesPlan categorySalesPlan) throws IOException, SQLException;
    void addCategorySalesPlan(CategorySalesPlan categorySalesPlan) throws IOException, SQLException;

    List<GeneralSalesPlan> getGeneralSalesPlanList() throws SQLException, IOException, ClassNotFoundException;
    GeneralSalesPlan getGeneralSalesPlanByQuarter(Quarter quarter) throws SQLException, IOException, ClassNotFoundException, IllegalQuarterException, IllegalYearException;
    void addGeneralSalesPlan(GeneralSalesPlan generalSalesPlan) throws IOException, SQLException;
    void updateGeneralSalesPlan(GeneralSalesPlan generalSalesPlan) throws IOException, SQLException;
}
