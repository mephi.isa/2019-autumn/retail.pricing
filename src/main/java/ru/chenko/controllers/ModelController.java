package ru.chenko.controllers;

import ru.chenko.Dto.CategoryDto;
import ru.chenko.Dto.ProductDto;
import ru.chenko.PersonnelDao.PersonnelDao;
import ru.chenko.PersonnelDao.PersonnelDaoStub;

import java.util.*;

public class ModelController {
    private PersonnelDao authController  = new PersonnelDaoStub();

    public Map<String,Object> fillInCategory(CategoryDto categoryDto, List<ProductDto> productDto){

        Map<String, Object> model = new HashMap<>();
        List<List> analysts = new ArrayList<>();
        List<List> products = new ArrayList<>();
        List<List> availableManagers = new ArrayList<>();
        List<List> availableAnalysts = new ArrayList<>();
        List<String> availableManagersStrings = authController.getManagersIDList();
        List<String> availableAnalystsStrings = authController.getAnalystsIDList();

        for(int i = 0; i < categoryDto.analystList.size();i++ ) {
            analysts.add(Arrays.asList(categoryDto.analystList.get(i).toString(),
                    authController.getUserFIO(categoryDto.analystList.get(i).toString())));
        }
        model.put("analysts", analysts);

        for (ProductDto dto : productDto) {
            products.add(Arrays.asList(dto.article, dto.name));
        }
        model.put("products", products);

        for (String availablePersonsString : availableManagersStrings) {
            availableManagers.add(Collections.singletonList(authController.getUserFIO(availablePersonsString)));
        }
        model.put("availableManagers",availableManagers);

        for (String availablePersonsString : availableAnalystsStrings) {
            availableAnalysts.add(Collections.singletonList(authController.getUserFIO(availablePersonsString)));
        }
        model.put("availableAnalysts",availableAnalysts);

        return model;
    }
}
