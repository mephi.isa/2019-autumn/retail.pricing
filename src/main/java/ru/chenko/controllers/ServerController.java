package ru.chenko.controllers;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import ru.chenko.Dto.CategoryDto;
import ru.chenko.Dto.CategorySalesPlanDto;
import ru.chenko.Dto.GeneralSalesPlanDto;
import ru.chenko.Dto.ProductDto;
import ru.chenko.PersonnelDao.PersonnelDao;
import ru.chenko.dao.Dao;
import ru.chenko.models.Status;
import ru.chenko.models.User;
import ru.chenko.services.Role;
import ru.chenko.services.Service;
import ru.chenko.services.ServiceImpl;
import ru.chenko.models.Quarter;
import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;
import java.util.*;

import static spark.Spark.*;
import static spark.Spark.staticFileLocation;

public class ServerController {
    private PersonnelDao authController;
    private ModelController modelController;
    private Dao dao;

    public ServerController(Dao dao,PersonnelDao personnelDao){
        this.dao = dao;
        authController = personnelDao;
        modelController = new ModelController();
    }

    public void run(){
        org.apache.log4j.BasicConfigurator.configure();
        port(8080);

        FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine();
        Configuration freemarkerConfiguration = new Configuration();
        freemarkerConfiguration.setTemplateLoader(new ClassTemplateLoader(ServerController.class, "/templates/"));
        freeMarkerEngine.setConfiguration(freemarkerConfiguration);
        staticFileLocation("/static");

        get("/", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if(user == null)
                response.redirect("/auth");
            if(user.role == Role.ANALYST){
                response.redirect("/analyst");
            }
            if(user.role == Role.MANAGER){
                response.redirect("/manager");
            }
            if(user.role == Role.SUPERVISOR){
                response.redirect("/supervisor");
            }
            return "success";
        });

        get("/auth", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            String login, password;
            login = request.queryParams("login");
            password= request.queryParams("password");
            if(authController.getUser(login,password) == null){
                model.put("status", "Wrong login or password");
                return freeMarkerEngine.render(new ModelAndView(model, "auth.ftl"));
            }
            response.cookie("/", "login", login, 10 * 50, false);
            response.cookie("/", "password", password, 10 * 50, false);
            response.redirect("/");
            return "success";
        });

        get("/logout", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            String cookie = request.cookie(null);
            return new ModelAndView(model, "auth");
        });


        get("/category", (request, response) -> {

            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();
            try {
                String categoryName = request.queryParams("categoryName");
                if (categoryName == null) {
                    categoryName = request.cookie("categoryName");
                }
                categoryName = categoryName.replaceAll("\\s","");
                response.cookie("/category", "categoryName", categoryName, 10 * 50, false);

                CategoryDto categoryDto = service.getCategory(categoryName);
                List<ProductDto> productsDto = service.getProductByCategory(categoryName);

                model = modelController.fillInCategory(categoryDto, productsDto);
                model.put("user", user.role.toString());
                model.put("manager", authController.getUserFIO(categoryDto.manager.toString()));
                model.put("categoryName", categoryName);
            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"category.ftl"));
        });

        get("/categorysalesplan", (request, response) -> {

            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");

            Service service = new ServiceImpl(dao,user);
            Map<String, Object> model = new HashMap<>();
            model.put("user",user.role.toString());
            try {
                String index = request.queryParams("index");
                String categoryName = request.queryParams("categoryName"+index);
                if(categoryName == null){
                    categoryName = request.cookie("categoryName");
                }
                String year = request.queryParams("year"+index);
                if(year == null){
                    year = request.cookie("year");
                }
                String quar = request.queryParams("quarter"+index);
                if(quar == null){
                    quar = request.cookie("quarter");
                }
                response.cookie("/categorysalesplan", "categoryName", categoryName, 10 * 50, false);
                response.cookie("/categorysalesplan", "year", year, 10 * 50, false);
                response.cookie("/categorysalesplan", "quarter", quar, 10 * 50, false);

                Quarter quarter = new Quarter(Integer.parseInt(quar), Integer.parseInt(year));

//                String categoryName = "Milk";
//                String year = "2019";
//                String quar = "4";
//                Quarter quarter = new Quarter(Integer.parseInt(quar), Integer.parseInt(year));

                CategorySalesPlanDto categorySalesPlanDto = service.getCategoriesSalesPlan(categoryName, quarter);


                model.put("status", categorySalesPlanDto.status.toString());
                model.put("categoryName", categorySalesPlanDto.categoryName);
                model.put("year", year);
                model.put("quarter", quar);


                List<List> products = new ArrayList<>();
                for (int i = 0; i < categorySalesPlanDto.salesPlanRowList.size(); i++) {
                    products.add(Arrays.asList(categorySalesPlanDto.salesPlanRowList.get(i).getProductArticle(),
                            service.getProductByArticle(categorySalesPlanDto.salesPlanRowList.get(i).getProductArticle()).name,
                            categorySalesPlanDto.salesPlanRowList.get(i).getCount().toString()));
                }
                model.put("products", products);
            }catch (Exception e){
                model.put("exception",e.getLocalizedMessage());
            }

            return freeMarkerEngine.render(new ModelAndView(model,"categorysalesplan.ftl"));
        });

        get("/generalsalesplan", (request, response) -> {

            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);
            CategorySalesPlanDto categorySalesPlanDto;
            Map<String, Object> model = new HashMap<>();

            try {
                String index = request.queryParams("index");
                String year = request.queryParams("year"+index);
                if(year == null){
                year = request.cookie("year");
                }
                String quar = request.queryParams("quarter"+index);
                if(quar == null){
                quar = request.cookie("quarter");
                }
                response.cookie("/generalsalesplan", "year", year, 10 * 50, false);
                response.cookie("/generalsalesplan", "quarter", quar, 10 * 50, false);

                Quarter quarter = new Quarter(Integer.parseInt(quar), Integer.parseInt(year));

                GeneralSalesPlanDto generalSalesPlanDto = service.getGeneralSalesPlanByQuarter(quarter);

                model.put("year", year);
                model.put("quarter", quar);
                model.put("user", user.role.toString());
                model.put("exception", request.cookie("exception"));
                response.cookie("/generalsalesplan", "exception", null, 10 * 50, false);

                List<List> products = new ArrayList<>();
                for (int i = 0; i < generalSalesPlanDto.categorySalesPlanList.size(); i++) {
                    categorySalesPlanDto = service.getCategoriesSalesPlan(generalSalesPlanDto.categorySalesPlanList.get(i), quarter);
                    products.add(Arrays.asList(categorySalesPlanDto.categoryName, categorySalesPlanDto.status));
                }
                model.put("plans", products);
            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }
            return freeMarkerEngine.render(new ModelAndView(model,"generalsalesplan.ftl"));
        });


        get("/analyst", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);

            Map<String, Object> model = new HashMap<>();

            List<List> plans = new ArrayList<>();

            try {
                List<CategorySalesPlanDto> categorySalesPlanDtoList = service.getCategorySalesPlanListByAnalyst(user.login);

                for (CategorySalesPlanDto categorySalesPlanDto : categorySalesPlanDtoList) {
                    if(categorySalesPlanDto.status != Status.ACCEPTED && categorySalesPlanDto.status != Status.COMPLETED)
                    plans.add(Arrays.asList(categorySalesPlanDto.categoryName,categorySalesPlanDto.quarter.getQuarter().toString(),categorySalesPlanDto.quarter.getYear().toString()
                            ,categorySalesPlanDto.status.toString()));
                }
                model.put("categorySalesPlans",plans);
                String analystID = authController.getUserFIO(user);
                model.put("analystID",analystID);
            }
            catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }

            return freeMarkerEngine.render(new ModelAndView(model,"analyst.ftl"));
        });

        get("/manager", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);

            Map<String, Object> model = new HashMap<>();
            List<List> categorySalesPlans = new ArrayList<>();
            List<List> categories = new ArrayList<>();

            try {
                List<CategorySalesPlanDto> categorySalesPlanDtoList = service.getCategorySalesPlanListByManager(user.login);
                List<CategoryDto> categoryDtoList = service.getCategoryListByManager(user.login);

                String FIO = authController.getUserFIO(user);
                model.put("managerID",FIO);

                for (CategorySalesPlanDto categorySalesPlanDto : categorySalesPlanDtoList) {
                    categorySalesPlans.add(Arrays.asList(categorySalesPlanDto.categoryName,
                            categorySalesPlanDto.quarter.getQuarter().toString(),
                            categorySalesPlanDto.quarter.getYear().toString(), categorySalesPlanDto.status.toString()));
                }
                model.put("categorySalesPlans", categorySalesPlans);

                for (CategoryDto categoryDto : categoryDtoList) {
                    categories.add(Arrays.asList(categoryDto.name, categoryDto.description));
                }
                model.put("categories", categories);
            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }

            return freeMarkerEngine.render(new ModelAndView(model,"manager.ftl"));
        });

        get("/supervisor", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);

            Map<String, Object> model = new HashMap<>();
            List<List> generalSalesPlans = new ArrayList<>();
            List<List> categoryList = new ArrayList<>();

            try {

                List<GeneralSalesPlanDto> generalSalesPlanDtoList = service.getGeneralSalesPlanList();
                List<CategoryDto> categoryDtoList = service.getCategoryList();

                String FIO = authController.getUserFIO(user);
                model.put("supervisorID",FIO);

                for (CategoryDto categoryDto : categoryDtoList) {
                    categoryList.add(Arrays.asList(categoryDto.name, categoryDto.description));
                }
                model.put("categories", categoryList);

                for (GeneralSalesPlanDto generalSalesPlanDto : generalSalesPlanDtoList) {
                    generalSalesPlans.add(Arrays.asList(generalSalesPlanDto.quarter.getQuarter().toString(),
                            generalSalesPlanDto.quarter.getYear().toString(),generalSalesPlanDto.status.toString()));
                }
                model.put("generalSalesPlans", generalSalesPlans);
            }catch (Exception e){
                model.put("exception", e.getLocalizedMessage());
            }

            return freeMarkerEngine.render(new ModelAndView(model,"supervisor.ftl"));
        });

        get("/addcategory", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try {
                Service service = new ServiceImpl(dao, user);
                String categoryName = request.queryParams("categoryName");
                String description = request.queryParams("description");
                service.addCategory(categoryName, description);
            }catch (Exception ignored){

            }
            response.redirect("/supervisor");
            return "success";
        });

        get("/addproduct", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try{
            Service service = new ServiceImpl(dao,user);
            String categoryName = request.queryParams("categoryName");
            String productArticle = request.queryParams("productArticle");
            String productName = request.queryParams("productName");
            service.addProduct(productArticle,productName,categoryName);
                }catch (Exception ignored){
                }
                response.redirect("/category");
            return "success";
        });

        get("/addanalyst", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try {
                Service service = new ServiceImpl(dao, user);
                String categoryName = request.queryParams("categoryName");
                String analystID = request.queryParams("analystID");
                analystID = authController.getUserByFIO(analystID);
                service.addAnalyst(categoryName, Long.decode(analystID));
            }catch (Exception ignored){
            }
            response.redirect("/category");
            return "success";
        });

        get("/remanalyst", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try {
                Service service = new ServiceImpl(dao, user);
                String categoryName = request.queryParams("categoryName");
                String analystID = request.queryParams("analystID");
                service.removeAnalyst(categoryName, Long.decode(analystID));
            }catch (Exception ignored){
            }
            response.redirect("/category");
            return "success";
        });

        get("/setmanager", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            try {
                Service service = new ServiceImpl(dao, user);
                String categoryName = request.queryParams("categoryName");
                String managerID = request.queryParams("managerID");
                managerID = authController.getUserByFIO(managerID);
                service.setManager(categoryName, Long.decode(managerID));
            }catch (Exception ignored){
            }
            response.redirect("/category");
            return "success";
        });


        get("/setproductquantity", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login,password);
            if(user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao,user);

            try {
                String categoryName = request.queryParams("categoryName");
                String quart = request.queryParams("quarter");
                String year = request.queryParams("year");
                String productArticle = request.queryParams("productArticle");
                String quantity = request.queryParams("quantity"+productArticle);
                Quarter quarter = new Quarter(Integer.parseInt(quart), Integer.parseInt(year));
                service.CategorySalesPlanSetProductQuantity(categoryName, quarter, productArticle, Integer.parseInt(quantity));
            }catch (Exception ignored){
            }
            response.redirect("/categorysalesplan");
            return "success";
        });

        get("/addgeneralplan", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if (user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao, user);

            String quart = request.queryParams("quarter");
            String year = request.queryParams("year");

            try {

                Quarter quarter = new Quarter(Integer.parseInt(quart), Integer.parseInt(year));
                service.addGeneralSalesPlan(quarter);
            }catch (Exception ignored){

            }
            response.redirect("/supervisor");
            return "success";
        });

        get("/completecategoryplan", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if (user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao, user);
            try {
                String index = request.queryParams("index");
                String categoryName = request.queryParams("categoryName"+index);
                String quart = request.queryParams("quarter"+index);
                String year = request.queryParams("year"+index);

                Quarter quarter = new Quarter(Integer.parseInt(quart), Integer.parseInt(year));
                service.CategorySalesPlanComplete(categoryName, quarter);
            }catch (Exception ignored){
            }
            response.redirect("/");
            return "success";
        });

        get("/acceptcategoryplan", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if (user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao, user);
            try {
                String index = request.queryParams("index");
                String categoryName = request.queryParams("categoryName"+index);
                String quart = request.queryParams("quarter"+index);
                String year = request.queryParams("year"+index);

                Quarter quarter = new Quarter(Integer.parseInt(quart), Integer.parseInt(year));
                service.CategorySalesPlanAccept(categoryName, quarter);
            }catch (Exception ignored){
            }
            if(user.role == Role.SUPERVISOR){
                response.redirect("/generalsalesplan");
            }
            response.redirect("/");
            return "success";
        });

        get("/rejectcategoryplan", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if (user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao, user);

            try {
                String index = request.queryParams("index");
                String categoryName = request.queryParams("categoryName"+index);
                String quart = request.queryParams("quarter"+index);
                String year = request.queryParams("year"+index);

                Quarter quarter = new Quarter(Integer.parseInt(quart), Integer.parseInt(year));
                service.CategorySalesPlanReject(categoryName, quarter);
            }catch (Exception ignored){
            }
            if(user.role == Role.SUPERVISOR){
                response.redirect("/generalsalesplan");
            }
            response.redirect("/");
            return "success";
        });

        get("/acceptgeneralsalesplan", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if (user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao, user);
            try {
                String index = request.queryParams("index");
                String quart = request.queryParams("quarter"+index);
                String year = request.queryParams("year"+index);

                Quarter quarter = new Quarter(Integer.parseInt(quart), Integer.parseInt(year));
                service.GeneralSalesPlanAccept(quarter);
            }catch (Exception ignored){
            }
            response.redirect("/");
            return "success";
        });

        get("/rejectgeneralsalesplan", (request, response) -> {
            String login = request.cookie("login");
            String password = request.cookie("password");
            User user = authController.getUser(login, password);
            if (user == null)
                response.redirect("/auth");
            Service service = new ServiceImpl(dao, user);

            try {
                String index = request.queryParams("index");
                String quart = request.queryParams("quarter"+index);
                String year = request.queryParams("year"+index);

                Quarter quarter = new Quarter(Integer.parseInt(quart), Integer.parseInt(year));
                service.GeneralSalesPlanReject(quarter);
            }catch (Exception ignored){
            }
            response.redirect("/");
            return "success";
        });

    }
}
