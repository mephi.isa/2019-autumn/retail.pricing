package ru.chenko.services;

import ru.chenko.Dto.*;
import ru.chenko.exceptions.*;
import ru.chenko.models.*;

import java.io.IOException;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public interface Service {

    CategoryDto getCategory(String categoryName) throws IOException, InvalidSessionException, SQLException, ClassNotFoundException;
    List<CategoryDto> getCategoryList() throws IOException, InvalidSessionException, SQLException, ClassNotFoundException;
    List<CategoryDto> getCategoryListByManager(Long manager) throws IOException, InvalidSessionException, SQLException, ClassNotFoundException;
    void addCategory(String name, String description) throws IOException, InvalidSessionException, InvalidDataException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException;
    void addAnalyst(String categoryName, Long analystPersonalNumber) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException;
    void removeAnalyst(String categoryName, Long analystPersonalNumber) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException;
    void removeProductFromCategory(String categoryName, String productArticle) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException;
    void setManager(String categoryName, Long manager) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException;

    ProductDto getProductByArticle(String productArticle) throws SQLException, IOException, ClassNotFoundException;
    List<ProductDto>  getProductByCategory(String categoryName) throws SQLException, IOException, ClassNotFoundException;
    List<ProductDto>  getProductSet() throws SQLException, IOException, ClassNotFoundException;
    public void addProduct(String productArticle,String productName, String categoryName) throws IOException, SQLException, InvalidDataException, EntitySavingException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException;

    CategorySalesPlanDto getCategoriesSalesPlan(String CategoryName,Quarter quarter) throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException;
    List<CategorySalesPlanDto> getCategorySalesPlanList() throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException;
    List<CategorySalesPlanDto> getCategorySalesPlanListByManager(Long manager) throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException;
    List<CategorySalesPlanDto> getCategorySalesPlanListByCategory(String CategoryName) throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException;
    List<CategorySalesPlanDto> getCategorySalesPlanListByAnalyst(Long Analyst) throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException;
    void CategorySalesPlanComplete(String categoryName,Quarter quarter) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException;
    void CategorySalesPlanAccept(String categoryName,Quarter quarter) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException;
    void CategorySalesPlanReject(String categoryName,Quarter quarter) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException;
    void CategorySalesPlanSetProductQuantity(String categoryName,Quarter quarter ,String productArticle,int quantity) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException;
    void CategorySalesPlanRemoveProduct(String categoryName,Quarter quarter,String productArticle) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException;

    List<GeneralSalesPlanDto> getGeneralSalesPlanList() throws IOException, InvalidSessionException, SQLException, ClassNotFoundException;
    GeneralSalesPlanDto getGeneralSalesPlanByQuarter(Quarter quarter) throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException;
    List<GeneralSalesPlanDto> getGeneralSalesPlanNotAccept() throws IOException, InvalidSessionException, SQLException, ClassNotFoundException;
    void addGeneralSalesPlan(Quarter quarter) throws IOException, InvalidSessionException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalCountException, IllegalQuarterException, IllegalYearException;
    void GeneralSalesPlanAccept(Quarter quarter) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException;
    void GeneralSalesPlanReject(Quarter quarter) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException;
    void GeneralSalesPlanRemoveCategorySalesPlan(Quarter quarter,String categoryName) throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException;

}
