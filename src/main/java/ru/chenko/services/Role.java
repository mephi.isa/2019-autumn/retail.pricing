package ru.chenko.services;

public enum Role {
    ANALYST,
    MANAGER,
    SUPERVISOR
}
