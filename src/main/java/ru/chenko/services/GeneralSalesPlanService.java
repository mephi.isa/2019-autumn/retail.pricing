package ru.chenko.services;

import ru.chenko.Dto.CategoryDto;
import ru.chenko.dao.*;
import ru.chenko.exceptions.*;
import  ru.chenko.Dto.GeneralSalesPlanDto;
import ru.chenko.models.Category;
import ru.chenko.models.GeneralSalesPlan;
import ru.chenko.models.Quarter;
import ru.chenko.models.User;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ru.chenko.models.Status.COMPLETED;
import static ru.chenko.services.Action.*;


public class GeneralSalesPlanService {
    private AuthorizationService authorizationService;
    private Dao dao;
    private User user;

    public GeneralSalesPlanService(Dao dao,User user) {
        this.user = user;
        this.dao = dao;
        this.authorizationService = new AuthorizationService();
    }

    public List<GeneralSalesPlan> getGeneralSalesPlanList()
            throws IOException, SQLException, ClassNotFoundException {
        boolean hasAccess = authorizationService.checkUserHasAccess(GENERAL_SALES_PLAN_GET, user);
        if(hasAccess)
            return dao.getGeneralSalesPlanList();
        return null;
    }

    public List<GeneralSalesPlan> getGeneralSalesPlanNotAccept()
            throws IOException, SQLException, ClassNotFoundException {
        List<GeneralSalesPlan> GeneralSalesPlanList = getGeneralSalesPlanList();
        List<GeneralSalesPlan> GeneralSalesPlanListOut = new ArrayList<>();

        for (GeneralSalesPlan generalSalesPlan : GeneralSalesPlanList) {
            if (generalSalesPlan.getStatus() == COMPLETED) {
                GeneralSalesPlanListOut.add(generalSalesPlan);
            }
        }
        return GeneralSalesPlanListOut;
    }

    public GeneralSalesPlan getGeneralSalesPlan(Quarter quarter )
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        boolean hasAccess = authorizationService.checkUserHasAccess(GENERAL_SALES_PLAN_GET, user);
        if(hasAccess)
            return dao.getGeneralSalesPlanByQuarter(quarter);
        return null;
    }

    public void addGeneralSalesPlan(Quarter quarter)
            throws IOException, InvalidDataException, SQLException {
        GeneralSalesPlan generalSalesPlan = new GeneralSalesPlan(quarter);
        boolean isGeneralSalesPlanValid  = ((generalSalesPlan.getStatus() != null) &&(generalSalesPlan.getQuarter() != null));

        if(!isGeneralSalesPlanValid){
            throw new InvalidDataException("Invalid data while saving General Sales Plan");
        }

        boolean hasAccess = authorizationService.checkUserHasAccess(GENERAL_SALES_PLAN_CREATE, user);
        if(hasAccess)
            dao.addGeneralSalesPlan(generalSalesPlan);
    }

    public void updateGeneralSalesPlan(GeneralSalesPlan generalSalesPlan,Action action)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        boolean isGeneralSalesPlanValid  = ((generalSalesPlan.getStatus() != null)&&(generalSalesPlan.getCategorySalesPlanSet() != null)
                &&(generalSalesPlan.getQuarter() != null));

        if(!isGeneralSalesPlanValid){
            throw new InvalidDataException("Invalid data while saving General Sales Plan");
        }

        boolean isGeneralSalesPlanExist = this.getGeneralSalesPlan(generalSalesPlan.getQuarter()) != null;

        if(!isGeneralSalesPlanExist)
            throw new EntitySavingException("Can`t update General Sales Plan. General Sales Plan with such name doesn`t exist");

        boolean hasAccess = authorizationService.checkUserHasAccess(action, user);
        if(hasAccess)
            dao.updateGeneralSalesPlan(generalSalesPlan);

    }

    public void GeneralSalesPlanComplete(Quarter quarter)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        GeneralSalesPlan categorySalesPlan = this.getGeneralSalesPlan(quarter);
        categorySalesPlan.complete();
        this.updateGeneralSalesPlan(categorySalesPlan,GENERAL_SALES_PLAN_GET);
    }

    public void GeneralSalesPlanAccept(Quarter quarter)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        GeneralSalesPlan generalSalesPlan = this.getGeneralSalesPlan(quarter);
        generalSalesPlan.accept();
        this.updateGeneralSalesPlan(generalSalesPlan,GENERAL_SALES_PLAN_ALP);
    }

    public void GeneralSalesPlanReject(Quarter quarter)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        GeneralSalesPlan categorySalesPlan = this.getGeneralSalesPlan(quarter);
        categorySalesPlan.reject();
        this.updateGeneralSalesPlan(categorySalesPlan,GENERAL_SALES_PLAN_ALP);

    }

    public void GeneralSalesPlanAddCategorySalesPlan(Quarter quarter,String categoryName)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        GeneralSalesPlan generalSalesPlan = this.getGeneralSalesPlan(quarter);
        generalSalesPlan.addCategorySalesPlan(categoryName);
        this.updateGeneralSalesPlan(generalSalesPlan,GENERAL_SALES_PLAN_GET);
    }

    public void GeneralSalesPlanRemoveCategorySalesPlan(Quarter quarter,String categoryName)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        GeneralSalesPlan categorySalesPlan = this.getGeneralSalesPlan(quarter);
        categorySalesPlan.removeCategorySalesPlan(categoryName);
        this.updateGeneralSalesPlan(categorySalesPlan,GENERAL_SALES_PLAN_GET);
    }

    public GeneralSalesPlanDto createGeneralSalesPlanDto(GeneralSalesPlan generalSalesPlan){
        GeneralSalesPlanDto generalSalesPlanDto = new GeneralSalesPlanDto();
        generalSalesPlanDto.categorySalesPlanList = new ArrayList<>(generalSalesPlan.getCategorySalesPlanSet());
        generalSalesPlanDto.quarter = generalSalesPlan.getQuarter();
        generalSalesPlanDto.status = generalSalesPlan.getStatus();
        return generalSalesPlanDto;
    }

    public List <GeneralSalesPlanDto> createGeneralSalesPlanDtoList(List<GeneralSalesPlan> generalSalesPlanList){

        List<GeneralSalesPlanDto> generalSalesPlanDtoList = new ArrayList<>();
        for (GeneralSalesPlan generalSalesPlan : generalSalesPlanList) {
            generalSalesPlanDtoList.add(createGeneralSalesPlanDto(generalSalesPlan));
        }
        return generalSalesPlanDtoList;
    }

}
