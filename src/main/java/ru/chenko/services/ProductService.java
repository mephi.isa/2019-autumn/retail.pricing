package ru.chenko.services;

import ru.chenko.Dto.ProductDto;
import ru.chenko.dao.Dao;
import ru.chenko.exceptions.IllegalPriceException;
import ru.chenko.models.Product;
import ru.chenko.models.User;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProductService {
    private Dao dao;
    private User user;

    public ProductService(Dao dao,User user){
        this.dao = dao;
        this.user = user;
    }

    ProductDto getProductByArticle(String productArticle) throws SQLException, IOException, ClassNotFoundException {
        return createProductDto(this.dao.getProductByArticle(productArticle));
    }

    List<ProductDto>  getProductList() throws SQLException, IOException, ClassNotFoundException {
        return createProductDtoList(dao.getProductList());
    }


    List<ProductDto>  getProductByCategory(String categoryName) throws SQLException, IOException, ClassNotFoundException {

        List<ProductDto> productDtoCategoryList = new ArrayList<>();
        List<ProductDto> productList = this.getProductList();

        for (ProductDto productDto : productList) {
            if (productDto.categoryName.equals(categoryName)) {
                productDtoCategoryList.add(productDto);
            }
        }

        return productDtoCategoryList;
    }

    public void updateProduct(Product product) throws IOException, SQLException {
        dao.updateProduct(product);
    }


    public void addProduct(String article,String productName, String categoryName) throws IOException, SQLException {
        Product product = new Product(article,productName,categoryName);
        dao.addProduct(product);
    }

    public List<ProductDto> createProductDtoList(List<Product> productSet){
        List<ProductDto> productDtoList = new ArrayList<>();

        for (Product product : productSet) {
            productDtoList.add(createProductDto(product));
        }

        return productDtoList;
    }

    public ProductDto createProductDto(Product product){

        ProductDto productDto = new ProductDto();
        productDto.article = product.getArticle();
        productDto.name = product.getName();
        productDto.categoryName = product.getCategoryName();
        return productDto;
    }


}
