package ru.chenko.services;

import ru.chenko.dao.Dao;
import ru.chenko.models.Category;
import ru.chenko.Dto.CategoryDto;

import ru.chenko.exceptions.*;
import ru.chenko.models.User;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


import static ru.chenko.services.Action.*;

public class CategoryService {
    private AuthorizationService authorizationService;
    private Dao dao;
    private User user;

    public CategoryService(Dao dao,User user){
        this.dao = dao;
        this.authorizationService = new AuthorizationService();
        this.user = user;
    }

    public List<Category> getCategoryList()
            throws IOException, SQLException, ClassNotFoundException {
        boolean hasAccess = authorizationService.checkUserHasAccess(CATEGORY_GET,user);
        if(hasAccess)
            return dao.getCategoryList();
        return null;
    }

    public List<Category> getCategoryListByManager(Long manager)
            throws IOException, SQLException, ClassNotFoundException {
        List<Category> categoryList = this.getCategoryList();
        List<Category> categoryListOut = new ArrayList<>();

        for (Category category : categoryList) {
            if (category.getManager().equals(manager)) {
                categoryListOut.add(category);
            }
        }
        return categoryListOut;
    }

    public Category getCategory(String CategoryName)
            throws IOException, SQLException, ClassNotFoundException {
        boolean hasAccess = authorizationService.checkUserHasAccess(CATEGORY_GET, user);
        if(hasAccess)
            return dao.getCategoryByName(CategoryName);
        return null;
    }

    public void addCategory(String name, String description)
            throws IOException, InvalidDataException, SQLException {
        Category category = new Category(name,description);
        boolean isCategoryValid = ((category.getName() != null) &&(category.getDescription() != null));
        if(!isCategoryValid){
            throw new InvalidDataException("Invalid data while saving category");
        }

        boolean hasAccess = authorizationService.checkUserHasAccess(CATEGORY_ADD, user);
        if(hasAccess)
            dao.addCategory(category);
    }

    public void updateCategory(Category category,Action action)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        boolean isCategoryValid = ((category.getName() != null)&&(category.getManager() != null)&&(category.getAnalystSet() != null)
                &&(category.getProductSet() != null)&&(category.getDescription() != null));
        if(!isCategoryValid){
            throw new InvalidDataException("Invalid data while saving category");
        }

        boolean isCategoryAlreadyExist = this.getCategory(category.getName()) != null;
        if(!isCategoryAlreadyExist)
            throw new EntitySavingException("Can`t update category. Category with such name doesn`t exist");


        boolean isOwner = (user.login.equals(category.getManager()));

        boolean hasAccess = authorizationService.checkUserHasAccess(action, user,isOwner);

        if(hasAccess)
            this.dao.updateCategory(category);
    }

    public void addAnalyst(String categoryName, Long analystPersonalNumber)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        Category category = this.getCategory(categoryName);
        category.addAnalyst(analystPersonalNumber);
        this.updateCategory(category,ANALYST_SET);
    }
    public void removeAnalyst(String categoryName, Long analystPersonalNumber)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        Category category = this.getCategory(categoryName);
        category.removeAnalyst(analystPersonalNumber);
        this.updateCategory(category,ANALYST_REM);
    }
    public void addProduct(String categoryName, String productArticle)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        Category category = this.getCategory(categoryName);
        category.addProduct(productArticle);
        this.updateCategory(category,CATEGORY_UPDATE);

    }
    public void removeProduct(String categoryName, String productArticle)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        Category category = this.getCategory(categoryName);
        category.addProduct(productArticle);
        this.updateCategory(category,CATEGORY_UPDATE);
    }
    public void setManager(String categoryName, Long manager)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        Category category = this.getCategory(categoryName);
        category.setManager(manager);
        this.updateCategory(category,CATEGORY_ADD);
    }

    public CategoryDto createCategoryDto(Category category){
        CategoryDto newCategoryDto = new CategoryDto();
        newCategoryDto.name = category.getName();
        newCategoryDto.description = category.getDescription();
        newCategoryDto.analystList = new ArrayList<>(category.getAnalystSet());
        newCategoryDto.manager = category.getManager();
        newCategoryDto.productArticlesList = new ArrayList<>(category.getProductSet());
        return newCategoryDto;
    }

    public List<CategoryDto> getCategoryDtoList(List<Category> category){
        List<CategoryDto> categoryDtoListOut = new ArrayList<>();

        for (Category value : category) {
            categoryDtoListOut.add(createCategoryDto(value));
        }
        return categoryDtoListOut;
    }


}
