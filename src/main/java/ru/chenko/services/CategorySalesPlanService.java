package ru.chenko.services;

import ru.chenko.dao.Dao;
import ru.chenko.Dto.CategorySalesPlanDto;
import ru.chenko.exceptions.*;
import ru.chenko.models.CategorySalesPlan;
import ru.chenko.models.Quarter;
import ru.chenko.models.User;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static ru.chenko.services.Action.CATEGORY_SALES_PLAN_GET;
import static ru.chenko.services.Action.CATEGORY_UPDATE;

public class CategorySalesPlanService {
    private AuthorizationService authorizationService;
    private CategoryService categoryService;
    private Dao dao;
    private User user;
    private Object Quarter;

    public CategorySalesPlanService(Dao dao,User user) {
        this.user = user;
        this.dao = dao;
        this.categoryService = new CategoryService(dao,user);
        this.authorizationService = new AuthorizationService();
    }

    public CategorySalesPlan getCategorySalesPlan(String CategoryName, Quarter quarter)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        boolean hasAccess = authorizationService.checkUserHasAccess(CATEGORY_SALES_PLAN_GET, user);
        if(hasAccess)
            return dao.getCategorySalesPlan(CategoryName, quarter);
        return null;
    }

    public List<CategorySalesPlan> getCategorySalesPlanList()
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {
        boolean hasAccess = authorizationService.checkUserHasAccess(CATEGORY_SALES_PLAN_GET, user);
        if(hasAccess)
            return dao.getCategorySalesPlanList();
        return null;
    }

    public List<CategorySalesPlan> getCategorySalesPlanListByCategory(String CategoryName)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {
        List<CategorySalesPlan> CategorySalesPlanList = this.getCategorySalesPlanList();
        List<CategorySalesPlan> CategorySalesPlanListOut = new ArrayList<>();

        for (CategorySalesPlan categorySalesPlan : CategorySalesPlanList) {
            if (categorySalesPlan.getCategory().equals(CategoryName)) { // LOL
                CategorySalesPlanListOut.add(categorySalesPlan);
            }
        }
        return CategorySalesPlanListOut;
    }

    public List<CategorySalesPlan> getCategorySalesPlanListByQuarter(Quarter quarter)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {
        List<CategorySalesPlan> CategorySalesPlanList = this.getCategorySalesPlanList();
        List<CategorySalesPlan> CategorySalesPlanListOut = new ArrayList<>();

        for (CategorySalesPlan categorySalesPlan : CategorySalesPlanList) {
            if (categorySalesPlan.getQuarter().getQuarter().equals(quarter.getQuarter())
                    &&categorySalesPlan.getQuarter().getYear().equals(quarter.getYear())) {
                CategorySalesPlanListOut.add(categorySalesPlan);
            }
        }
        return CategorySalesPlanListOut;
    }

    public List<CategorySalesPlan> getCategorySalesPlanListByManager(Long manager)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {

        List<CategorySalesPlan> CategorySalesPlanList = this.getCategorySalesPlanList();
        List<CategorySalesPlan> CategorySalesPlanListOut = new ArrayList<>();

        for (CategorySalesPlan categorySalesPlan : CategorySalesPlanList) {
            if (this.categoryService.getCategory(categorySalesPlan.getCategory()).getManager().equals(manager)) { // LOL
                CategorySalesPlanListOut.add(categorySalesPlan);
            }
        }
        return CategorySalesPlanListOut;
    }

    public List<CategorySalesPlan> getCategorySalesPlanListByAnalyst(Long analyst)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {

        List<CategorySalesPlan> CategorySalesPlanList = this.getCategorySalesPlanList();
        List<CategorySalesPlan> CategorySalesPlanListOut = new ArrayList<>();

        for (CategorySalesPlan categorySalesPlan : CategorySalesPlanList) {
            if (this.categoryService.getCategory(categorySalesPlan.getCategory()).getAnalystSet().contains(analyst)) {
                CategorySalesPlanListOut.add(categorySalesPlan);
            }
        }
        return CategorySalesPlanListOut;
    }

    public void addCategorySalesPlan(Quarter quarter, String categoryName)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {

        CategorySalesPlan categorySalesPlan = new CategorySalesPlan(quarter,categoryName);

        List<String> productList = new ArrayList<>(this.categoryService.getCategory(categoryName).getProductSet());

        for (String s : productList) {
            categorySalesPlan.setCount(s, 0);
        }

        boolean isCategorySalesPlan = ((categorySalesPlan.getCategory()!= null)&&(categorySalesPlan.getQuarter() != null));

        if(!isCategorySalesPlan){
            throw new InvalidDataException("Invalid data while saving Category Sales Plan");
        }

        boolean isPlanExist = false;

        List<CategorySalesPlan> CategorySalesPlanList = this.getCategorySalesPlanList();

        for (CategorySalesPlan salesPlan : CategorySalesPlanList) {
            if ((salesPlan.getQuarter() == categorySalesPlan.getQuarter())) {
                isPlanExist = true;
                break;
            }
        }
        if(isPlanExist){
            throw new InvalidDataException("Invalid data category sales plan this Quarter is already exist");
        }

        boolean hasAccess = authorizationService.checkUserHasAccess(CATEGORY_SALES_PLAN_GET, user);
        if(hasAccess)
            dao.addCategorySalesPlan(categorySalesPlan);
    }

    public void updateCategorySalesPlan(CategorySalesPlan categorySalesPlan,Action action)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalCountException, IllegalYearException {

        boolean isCategorySalesPlan = ((categorySalesPlan.getCategory()!= null)&&(categorySalesPlan.getSalesPlanRowSet() != null)
                &&(categorySalesPlan.getQuarter() != null));

        if(!isCategorySalesPlan){
            throw new InvalidDataException("Invalid data while saving Category Sales Plan");
        }

        boolean isPlanExist = false;

        List<CategorySalesPlan> CategorySalesPlanList = this.getCategorySalesPlanList();

        for (CategorySalesPlan salesPlan : CategorySalesPlanList) {
            if ((salesPlan.getQuarter().getYear().equals(categorySalesPlan.getQuarter().getYear()))&& (salesPlan.getQuarter().getQuarter().equals(categorySalesPlan.getQuarter().getQuarter()))) {
                isPlanExist = true;
                break;
            }
        }
        if(!isPlanExist){
            throw new EntitySavingException("Can`t update Category Sales Plan.Category Sales Plan with such name doesn`t exist");
        }

        boolean hasAccess = authorizationService.checkUserHasAccess(action, user);
        if(hasAccess)
            dao.updateCategorySalesPlan(categorySalesPlan);
    }

    public void CategorySalesPlanComplete(String categoryName,Quarter quarter)
            throws IOException, InvalidDataException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        CategorySalesPlan categorySalesPlan = this.getCategorySalesPlan(categoryName,quarter);
        categorySalesPlan.complete();
        this.updateCategorySalesPlan(categorySalesPlan,CATEGORY_SALES_PLAN_GET);

    }

    public void CategorySalesPlanAccept(String categoryName,Quarter quarter)
            throws IOException, InvalidDataException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        CategorySalesPlan categorySalesPlan = this.getCategorySalesPlan(categoryName,quarter);
        categorySalesPlan.accept();
        this.updateCategorySalesPlan(categorySalesPlan,CATEGORY_UPDATE);
    }

    public void CategorySalesPlanReject(String categoryName,Quarter quarter)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        CategorySalesPlan categorySalesPlan = this.getCategorySalesPlan(categoryName,quarter);
        categorySalesPlan.reject();
        this.updateCategorySalesPlan(categorySalesPlan,CATEGORY_UPDATE);
    }

    public void CategorySalesPlanSetCount(String categoryName, Quarter quarter , String productArticle,int quantity)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        CategorySalesPlan categorySalesPlan = this.getCategorySalesPlan(categoryName,quarter);
        categorySalesPlan.setCount(productArticle,quantity);
        this.updateCategorySalesPlan(categorySalesPlan,CATEGORY_SALES_PLAN_GET);
    }

    public void CategorySalesPlanRemoveProduct(String categoryName,Quarter quarter ,String product)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        CategorySalesPlan categorySalesPlan = this.getCategorySalesPlan(categoryName,quarter);
        categorySalesPlan.removeProduct(product);
        this.updateCategorySalesPlan(categorySalesPlan,CATEGORY_SALES_PLAN_GET);
    }

    public CategorySalesPlanDto createCategorySalesPlanDto(CategorySalesPlan categorySalesPlan){
        CategorySalesPlanDto categorySalesPlanDto = new CategorySalesPlanDto();
        categorySalesPlanDto.categoryName = categorySalesPlan.getCategory();
        categorySalesPlanDto.quarter = categorySalesPlan.getQuarter();
        categorySalesPlanDto.salesPlanRowList = new ArrayList<>(categorySalesPlan.getSalesPlanRowSet().values());
        categorySalesPlanDto.status = categorySalesPlan.getStatus();
        return categorySalesPlanDto;
    }

    public List <CategorySalesPlanDto> createCategorySalesPlanDtoList(List<CategorySalesPlan> categorySalesPlanList){

        List<CategorySalesPlanDto> categorySalesPlanDtoList = new ArrayList<>();

        for (CategorySalesPlan categorySalesPlan : categorySalesPlanList) {
            categorySalesPlanDtoList.add(createCategorySalesPlanDto(categorySalesPlan));
        }
        return categorySalesPlanDtoList;
    }

}
