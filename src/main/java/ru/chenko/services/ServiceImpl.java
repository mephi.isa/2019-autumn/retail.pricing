package ru.chenko.services;

import ru.chenko.Dto.CategoryDto;
import ru.chenko.Dto.CategorySalesPlanDto;
import ru.chenko.Dto.GeneralSalesPlanDto;
import ru.chenko.Dto.ProductDto;
import ru.chenko.dao.Dao;
import ru.chenko.exceptions.*;
import ru.chenko.models.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class ServiceImpl implements Service  {

    private CategoryService categoryService;
    private CategorySalesPlanService categorySalesPlanService;
    private GeneralSalesPlanService generalSalesPlanService;
    private ProductService productService;
    User user;


    public ServiceImpl(Dao dao,User user) {
        this.user = user;
        this.categoryService = new CategoryService(dao,user);
        this.categorySalesPlanService = new CategorySalesPlanService(dao,user);
        this.generalSalesPlanService = new GeneralSalesPlanService(dao,user);
        this.productService = new ProductService(dao,user);
    }

    @Override
    public CategoryDto getCategory(String categoryName) throws IOException, SQLException, ClassNotFoundException {
        return this.categoryService.createCategoryDto(this.categoryService.getCategory(categoryName));
    }

    @Override
    public
    List<CategoryDto> getCategoryList() throws IOException, SQLException, ClassNotFoundException {
        return this.categoryService.getCategoryDtoList(this.categoryService.getCategoryList());
    }

    @Override
    public List<CategoryDto> getCategoryListByManager(Long manager)
            throws IOException, SQLException, ClassNotFoundException {
        return this.categoryService.getCategoryDtoList(this.categoryService.getCategoryListByManager(manager));
    }

    @Override
    public void addCategory(String name, String description)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        this.categoryService.addCategory(name,description);

        List<GeneralSalesPlan> generalSalesPlanList = this.generalSalesPlanService.getGeneralSalesPlanList();

        //Как учитывать планы в прошлом? Сбрасывать ли флаги готовности или не включать в уже принятые General планы новые
        //category планы?

        for (GeneralSalesPlan generalSalesPlan : generalSalesPlanList) {
            this.categorySalesPlanService.addCategorySalesPlan(generalSalesPlan.getQuarter(), name);
        }

    }

    @Override
    public void addAnalyst(String categoryName, Long analystPersonalNumber)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        this.categoryService.addAnalyst(categoryName,analystPersonalNumber);
    }

    @Override
    public void removeAnalyst(String categoryName, Long analystPersonalNumber)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        this.categoryService.removeAnalyst(categoryName,analystPersonalNumber);
    }

    @Override
    public void removeProductFromCategory(String categoryName, String productArticle)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        this.categoryService.removeProduct(categoryName,productArticle);
    }

    @Override
    public void setManager(String categoryName, Long manager)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException {
        this.categoryService.setManager(categoryName,manager);
    }

    @Override
    public ProductDto getProductByArticle(String productArticle) throws SQLException, IOException, ClassNotFoundException {
        return this.productService.getProductByArticle(productArticle);
    }

    @Override
    public List<ProductDto> getProductByCategory(String categoryName) throws SQLException, IOException, ClassNotFoundException{
        return this.productService.getProductByCategory(categoryName);
    }

    @Override
    public List<ProductDto>  getProductSet() throws SQLException, IOException, ClassNotFoundException {
        return this.productService.getProductList();
    }

    @Override
    public void addProduct(String productArticle,String productName, String categoryName) throws IOException, SQLException, InvalidDataException, EntitySavingException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        this.categoryService.addProduct(categoryName,productArticle);
        this.productService.addProduct(productArticle,productName,categoryName);
        List<CategorySalesPlan> categorySalesPlanList = this.categorySalesPlanService.
                getCategorySalesPlanListByCategory(categoryName);

        for (CategorySalesPlan categorySalesPlan : categorySalesPlanList) {
            this.categorySalesPlanService.CategorySalesPlanSetCount(categoryName,
                    categorySalesPlan.getQuarter(), productArticle, 0);
        }
    }

    @Override
    public CategorySalesPlanDto getCategoriesSalesPlan(String CategoryName, Quarter quarter)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        return this.categorySalesPlanService.createCategorySalesPlanDto
                (this.categorySalesPlanService.getCategorySalesPlan(CategoryName,quarter));
    }

    @Override
    public List<CategorySalesPlanDto> getCategorySalesPlanList()
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {
        return this.categorySalesPlanService.createCategorySalesPlanDtoList
                (this.categorySalesPlanService.getCategorySalesPlanList());
    }

    @Override
    public List<CategorySalesPlanDto> getCategorySalesPlanListByCategory(String CategoryName)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {
        return this.categorySalesPlanService.createCategorySalesPlanDtoList
                (this.categorySalesPlanService.getCategorySalesPlanListByCategory(CategoryName));
    }

    @Override
    public List<CategorySalesPlanDto> getCategorySalesPlanListByManager(Long manager)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {
        return this.categorySalesPlanService.createCategorySalesPlanDtoList
                (this.categorySalesPlanService.getCategorySalesPlanListByManager(manager));
    }

    @Override
    public void CategorySalesPlanComplete(String categoryName,Quarter quarter)
            throws IOException, InvalidDataException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        this.categorySalesPlanService.CategorySalesPlanComplete(categoryName,quarter);

        List<CategorySalesPlan> categorySalesPlans = this.categorySalesPlanService.getCategorySalesPlanListByQuarter(quarter);

        boolean allCompletedFlag = true;
        for (CategorySalesPlan categorySalesPlan : categorySalesPlans) {
            if (categorySalesPlan.getStatus() != Status.COMPLETED) {
                allCompletedFlag = false;
                break;
            }
        }
        if(allCompletedFlag){
            this.generalSalesPlanService.GeneralSalesPlanComplete(quarter);
        }
    }

    @Override
    public List<CategorySalesPlanDto> getCategorySalesPlanListByAnalyst(Long analyst)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalCountException, IllegalYearException {
        return this.categorySalesPlanService.createCategorySalesPlanDtoList
                (this.categorySalesPlanService.getCategorySalesPlanListByAnalyst(analyst));
    }

    @Override
    public void CategorySalesPlanAccept(String categoryName,Quarter quarter)
            throws IOException, InvalidDataException, IllegalSalesPlanStateException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalYearException, IllegalCountException {

        this.categorySalesPlanService.CategorySalesPlanAccept(categoryName,quarter);

        List<CategorySalesPlan> categorySalesPlans = this.categorySalesPlanService.getCategorySalesPlanListByQuarter(quarter);
        boolean allAcceptFlag = true;

        for (CategorySalesPlan categorySalesPlan : categorySalesPlans) {
            if (categorySalesPlan.getStatus() != Status.ACCEPTED) {
                allAcceptFlag = false;
                break;
            }
        }

        if(allAcceptFlag)
            this.generalSalesPlanService.GeneralSalesPlanComplete(quarter);
    }

    @Override
    public void CategorySalesPlanReject(String categoryName,Quarter quarter)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        this.categorySalesPlanService.CategorySalesPlanReject(categoryName,quarter);
    }

    @Override
    public void CategorySalesPlanSetProductQuantity(String categoryName,Quarter quarter ,String productArticle, int quantity)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        this.categorySalesPlanService.CategorySalesPlanSetCount(categoryName,quarter,productArticle,quantity);
    }

    @Override
    public void CategorySalesPlanRemoveProduct(String categoryName,Quarter quarter ,String productArticle)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        this.categorySalesPlanService.CategorySalesPlanRemoveProduct(categoryName,quarter,productArticle);
    }

    @Override
    public List<GeneralSalesPlanDto> getGeneralSalesPlanList()
            throws IOException, SQLException, ClassNotFoundException {
        return this.generalSalesPlanService.createGeneralSalesPlanDtoList
                (this.generalSalesPlanService.getGeneralSalesPlanList());
    }

    @Override
    public GeneralSalesPlanDto getGeneralSalesPlanByQuarter(Quarter quarter)
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        return this.generalSalesPlanService.createGeneralSalesPlanDto
                (this.generalSalesPlanService.getGeneralSalesPlan(quarter));
    }
    @Override
    public List<GeneralSalesPlanDto> getGeneralSalesPlanNotAccept()
            throws IOException, SQLException, ClassNotFoundException {
        return this.generalSalesPlanService.createGeneralSalesPlanDtoList
                (this.generalSalesPlanService.getGeneralSalesPlanNotAccept());
    }

    @Override
    public void addGeneralSalesPlan(Quarter quarter)
            throws IOException, InvalidDataException, SQLException, ClassNotFoundException, EntitySavingException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        this.generalSalesPlanService.addGeneralSalesPlan(quarter);

        List<Category> categoryList = categoryService.getCategoryList();
        for (Category category : categoryList) {
            this.categorySalesPlanService.addCategorySalesPlan(quarter, category.getName());
            this.generalSalesPlanService.GeneralSalesPlanAddCategorySalesPlan(quarter,category.getName());
        }

    }

    @Override
    public void GeneralSalesPlanAccept(Quarter quarter)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        this.generalSalesPlanService.GeneralSalesPlanAccept(quarter);
    }

    @Override
    public void GeneralSalesPlanReject(Quarter quarter)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalSalesPlanStateException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        this.generalSalesPlanService.GeneralSalesPlanReject(quarter);

        List<CategorySalesPlan> categorySalesPlans = this.categorySalesPlanService.getCategorySalesPlanListByQuarter(quarter);

        for (CategorySalesPlan categorySalesPlan : categorySalesPlans) {
            this.CategorySalesPlanReject(categorySalesPlan.getCategory(), quarter);
        }
    }

    @Override
    public void GeneralSalesPlanRemoveCategorySalesPlan(Quarter quarter,String categoryName)
            throws IOException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        this.generalSalesPlanService.GeneralSalesPlanRemoveCategorySalesPlan(quarter,categoryName);
    }

}
