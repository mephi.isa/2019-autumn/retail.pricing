package ru.chenko.services;

import ru.chenko.models.User;

public class AuthorizationService {
     boolean checkUserHasAccess(Action action,User user, Boolean isOwner) {
        Role userRole = user.role;
        switch (userRole) {
            case SUPERVISOR:
                return true;
            case MANAGER:
                if(action == Action.MANAGER_SET || action == Action.MANAGER_REM || action == Action.MANAGER_LIST_GET
                        || action == Action.GENERAL_SALES_PLAN_ALP){
                    return false;
                }
                if(action == Action.GENERAL_SALES_PLAN_CREATE || action == Action.ANALYST_REM || action == Action.ANALYST_SET
                        || action == Action.ANALYST_LIST_GET || action == Action.CATEGORY_SALES_PLAN_APL
                        || action == Action.CATEGORY_SALES_PLAN_GET||action == Action.CATEGORY_ADD)
                    return isOwner;
                else
                    return true;
            case ANALYST:
                if(action == Action.CATEGORY_GET || action == Action.PRODUCT_ADD || action == Action.CATEGORY_SALES_PLAN_CREATE || action == Action.CATEGORY_SALES_PLAN_GET)
                    return isOwner;
                else
                    return false;
        }
        return false;
    }

    boolean checkUserHasAccess(Action action, User user){
        return this.checkUserHasAccess(action,user, true);
    }

}
