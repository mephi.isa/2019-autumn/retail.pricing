package ru.chenko.models;

import ru.chenko.Dto.ProductDto;
import ru.chenko.exceptions.IllegalPriceException;
import java.io.Serializable;


public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    private String article;
    private String name;
    private String categoryName;

    public Product(String article,String productName , String categoryName) {
        this.article = article;
        this.name = productName;
        this.categoryName = categoryName;
    }

    public Product(ProductDto productDto){
        this.article = productDto.article;
        this.name = productDto.name;
        this.categoryName = productDto.categoryName;
    }

    public String getArticle() {
        return article;
    }

    public String getName(){return name;}

    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return article.equals(product.article);
    }


}
