package ru.chenko.models;

import ru.chenko.exceptions.IllegalQuarterException;
import ru.chenko.exceptions.IllegalYearException;

import java.io.Serializable;
import java.util.Objects;

public class Quarter implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer quarter;
    private Integer year;

    public Quarter(Integer quarter, Integer year) throws IllegalQuarterException, IllegalYearException {
        if(quarter >= 1 && quarter <= 4)
            this.quarter = quarter;
        else
            throw new IllegalQuarterException("Quarter can be in range of 1 to 4");
        if(year >= 1970 && year <= 2099)
            this.year = year;
        else
            throw new IllegalYearException("Year can be in range of 1970 to 2099");
    }

    public Integer getQuarter() {
        return quarter;
    }

    public Integer getYear() {
        return year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Quarter quarter1 = (Quarter) o;
        return quarter.equals(quarter1.quarter) &&
                year.equals(quarter1.year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quarter, year);
    }
}
