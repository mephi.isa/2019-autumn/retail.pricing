package ru.chenko.models;
import ru.chenko.Dto.CategoryDto;

import java.io.Serializable;


import java.util.HashSet;
import java.util.Objects;


public class Category implements Serializable {

    private static final long serialVersionUID =1L;
    private String name;
    private String description;
    private Long manager = 0L;
    private HashSet<Long> analystList;
    private HashSet<String> productList;

    public Category(String name, String description) {
        this.name = name;
        this.description = description;
        analystList = new HashSet<>();
        productList = new HashSet<>();
    }

    public Category(CategoryDto categoryDto){
        this.name = categoryDto.name;
        this.description = categoryDto.description;
        this.manager = categoryDto.manager;
        this.analystList = new HashSet<>(categoryDto.analystList);
        this.productList = new HashSet<>(categoryDto.productArticlesList);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setManager(Long manager) {
        this.manager = manager;
    }

    public Long getManager() {
        return manager;
    }

    public void addAnalyst(Long analystPersonalNumber) {
        analystList.add(analystPersonalNumber);
    }

    public void removeAnalyst(Long analystPersonalNumber) {
        analystList.remove(analystPersonalNumber);
    }

    public HashSet<Long> getAnalystSet() {
        return analystList;
    }

    public void addProduct(String article) {
        productList.add(article);
    }

    public void removeProduct(Product product) {
        productList.remove(product.getArticle());
    }

    public HashSet<String> getProductSet() {
        return productList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return name.equals(category.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
