package ru.chenko.models;

import ru.chenko.exceptions.IllegalCountException;

import java.io.Serializable;


public class SalesPlanRow implements Serializable {
    private static final long serialVersionUID = 1L;
    private String productArticle;
    private Integer count;

    public SalesPlanRow(String product) {
        this.productArticle = product;
        this.count = 0;
    }

    public String getProductArticle() {
        return productArticle;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) throws IllegalCountException {
        if(count >= 0)
            this.count = count;
        else
            throw new IllegalCountException("Product count in sales plan cant be less than 0");
    }

}
