package ru.chenko.models;
import ru.chenko.Dto.GeneralSalesPlanDto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class GeneralSalesPlan implements Serializable {
    private static final long serialVersionUID = 1L;
    private Quarter quarter;
    private Status status;
    private Set<String> categorySalesPlanSet;

    public GeneralSalesPlan(Quarter quarter) {
        this.quarter = quarter;
        this.status = Status.CREATED;
        categorySalesPlanSet = new HashSet<>();
    }

    public GeneralSalesPlan(GeneralSalesPlanDto generalSalesPlanDto) {
        this.quarter = generalSalesPlanDto.quarter;
        this.status = generalSalesPlanDto.status;
        categorySalesPlanSet = new HashSet<>(generalSalesPlanDto.categorySalesPlanList);
    }

    public Quarter getQuarter() {
        return quarter;
    }

    public void complete() {
           this.status = Status.COMPLETED;
    }

    public void accept()  {
            this.status = Status.ACCEPTED;
    }

    public void reject()  {
            this.status = Status.REJECTED;
        }

    public Status getStatus() {
        return status;
    }

    public void addCategorySalesPlan(String CategoryName) {
        categorySalesPlanSet.add(CategoryName);
    }

    public void removeCategorySalesPlan(String CategoryName) {
        categorySalesPlanSet.remove(CategoryName);
    }

    public Set<String> getCategorySalesPlanSet(){
        return this.categorySalesPlanSet;
    }

}
