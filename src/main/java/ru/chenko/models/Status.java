package ru.chenko.models;

import java.io.Serializable;

public enum Status implements Serializable {
    CREATED,
    COMPLETED,
    ACCEPTED,
    REJECTED
}
