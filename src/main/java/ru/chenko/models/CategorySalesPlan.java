package ru.chenko.models;

import com.beust.jcommander.internal.Nullable;
import ru.chenko.Dto.CategorySalesPlanDto;
import ru.chenko.exceptions.IllegalCountException;
import ru.chenko.exceptions.IllegalSalesPlanStateException;

import java.io.Serializable;
import java.util.*;

public class CategorySalesPlan implements Serializable {
    private static final long serialVersionUID = 1L;
    private Quarter quarter;
    private Status status;
    private Map<String, SalesPlanRow> salesPlanRowList = new HashMap<>();
    private String categoryName;

    public CategorySalesPlan(Quarter quarter, String categoryName) {
        this.quarter = quarter;
        this.categoryName = categoryName;
        this.status = Status.CREATED;
    }

    public CategorySalesPlan(CategorySalesPlanDto categorySalesPlanDto){
        this.quarter = categorySalesPlanDto.quarter;
        this.status = categorySalesPlanDto.status;
        this.categoryName = categorySalesPlanDto.categoryName;

        for (int i = 0; i < categorySalesPlanDto.salesPlanRowList.size();i++){
            salesPlanRowList.put(categorySalesPlanDto.salesPlanRowList.get(i).getProductArticle(),
                    categorySalesPlanDto.salesPlanRowList.get(i));
        }

    }

    public Quarter getQuarter() {
        return quarter;
    }

    public void complete() throws IllegalSalesPlanStateException {
        boolean isAllCountSetUp = salesPlanRowList.size() > 0;
        if(isAllCountSetUp && this.status != Status.ACCEPTED)
            this.status = Status.COMPLETED;
        else
            throw new IllegalSalesPlanStateException("You can complete only sales plan with all product counts more than 0");
    }

    public void accept() throws IllegalSalesPlanStateException {
        if(status == Status.COMPLETED)
            this.status = Status.ACCEPTED;
        else
            throw new IllegalSalesPlanStateException("You can reject only completed sales plan");
    }

    public void reject() {
            this.status = Status.REJECTED;
    }

    public Status getStatus() {
        return status;
    }

    public String getCategory() {
        return categoryName;
    }

    public void setCount(String productArticle,int quantity) throws IllegalCountException {
        SalesPlanRow salesPlanRow;
        if(salesPlanRowList.containsKey(productArticle)){
             salesPlanRow = salesPlanRowList.get(productArticle);
            salesPlanRowList.remove(productArticle);
        }else{
            salesPlanRow = new SalesPlanRow(productArticle);
        }
        salesPlanRow.setCount(quantity);
        salesPlanRowList.put(productArticle,salesPlanRow);
    }

    public void removeProduct(String productArticle) {
        salesPlanRowList.remove(productArticle);
    }

    public Map<String, SalesPlanRow> getSalesPlanRowSet() {
        return  salesPlanRowList;
    }

    public void addProduct(String article) {
    }
}
