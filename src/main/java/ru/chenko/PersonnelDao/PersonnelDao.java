package ru.chenko.PersonnelDao;
import ru.chenko.exceptions.InvalidCredentialsException;
import ru.chenko.models.User;
import java.util.List;

public interface PersonnelDao {
    User getUser(String Login, String password) throws InvalidCredentialsException;
    String getUserFIO(User user);
    String getUserFIO(String user);
    String getUserByFIO(String fio);
    List<String> getUsersIDList();
    List<String> getSupervisorsIDList();
    List<String> getManagersIDList();
    List<String> getAnalystsIDList();
}