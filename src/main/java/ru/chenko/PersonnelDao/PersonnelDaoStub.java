package ru.chenko.PersonnelDao;

import ru.chenko.exceptions.InvalidCredentialsException;
import ru.chenko.models.User;
import ru.chenko.services.Role;

import java.util.ArrayList;
import java.util.List;

public class PersonnelDaoStub implements PersonnelDao {
    @Override
    public User getUser(String login, String password) throws InvalidCredentialsException {
        User user = new User();
        if(login == null || password == null){
            return null;
        }
        if(login.equals("SUPERVISOR") && password.equals("SUPERVISOR")){
            user.login = 123L;
            user.role = Role.SUPERVISOR;
            return user;
        }
        if(login.equals("MANAGER") && password.equals("MANAGER")){
            user.login = 124L;
            user.role = Role.MANAGER;
            return user;
        }
        if(login.equals("ANALYST") && password.equals("ANALYST")){
            user.login = 125L;
            user.role = Role.ANALYST;
            return user;
        }
        return null;
    }

    @Override
    public String getUserFIO(User user){
        if (user.login == 123) {return "Иванов Иван Иванович";
        } else if (user.login == 124) {return "Василий Васильевич Васильев";
        } else if (user.login == 125) {return "Петров Петр Петрович";
        } else if (user.login == 126) {return "Петров Иван Петрович";
        } else if (user.login == 127) {return "Петров Василий Петрович";
        }
        return "Некто";
    }

    @Override
    public String getUserFIO(String user){
        if (Integer.parseInt(user) == 123) {return "Иванов Иван Иванович";
        } else if (Integer.parseInt(user) == 124) {return "Василий Васильевич Васильев";
        } else if (Integer.parseInt(user) == 125) {return "Петров Петр Петрович";
        } else if (Integer.parseInt(user) == 126) {return "Петров Иван Петрович";
        } else if (Integer.parseInt(user) == 127) {return "Петров Василий Петрович";
        }
        return "Некто";
    }

    @Override
    public List<String> getUsersIDList(){
        List<String> stringList = new ArrayList<>();
        stringList.add("123");
        stringList.add("124");
        stringList.add("125");
        stringList.add("126");
        stringList.add("127");
        return stringList;
    }

    @Override
    public List<String> getSupervisorsIDList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("123");
        return stringList;
    }

    @Override
    public List<String> getManagersIDList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("124");
        return stringList;
    }

    @Override
    public List<String> getAnalystsIDList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("125");
        stringList.add("126");
        stringList.add("127");
        return stringList;
    }

    @Override
    public String getUserByFIO(String FIO){
        if (FIO.equals("Иванов Иван Иванович")) {return "123";
        } else if (FIO.equals("Василий Васильевич Васильев")) {return "124";
        } else if (FIO.equals("Петров Петр Петрович")) {return "125";
        } else if (FIO.equals("Петров Иван Петрович")) {return "126";
        } else if (FIO.equals("Петров Василий Петрович")) {return "127";
        }
        return "Некто";
    }
}
