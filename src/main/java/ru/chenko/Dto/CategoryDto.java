package ru.chenko.Dto;
import java.util.ArrayList;
import java.util.List;


public class CategoryDto {
     public String name;
     public String description;
     public Long manager;
     public List<Long> analystList;
     public List<String> productArticlesList;
}
