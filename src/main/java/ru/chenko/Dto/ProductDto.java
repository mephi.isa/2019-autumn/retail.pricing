package ru.chenko.Dto;

public class ProductDto {
    public String article;
    public String name;
    public String description;
    public String categoryName;
}
