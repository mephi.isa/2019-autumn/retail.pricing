package ru.chenko.Dto;

import ru.chenko.models.Quarter;
import ru.chenko.models.SalesPlanRow;
import ru.chenko.models.Status;

import java.util.ArrayList;
import java.util.List;

public class CategorySalesPlanDto {
    public Quarter quarter;
    public Status status;
    public String categoryName;
    public List<SalesPlanRow> salesPlanRowList = new ArrayList<>();
}
