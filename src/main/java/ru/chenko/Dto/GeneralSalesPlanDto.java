package ru.chenko.Dto;
import ru.chenko.models.Quarter;
import ru.chenko.models.Status;

import java.util.ArrayList;
import java.util.List;

public class GeneralSalesPlanDto {
    public Quarter quarter;
    public Status status;
    public List<String> categorySalesPlanList = new ArrayList<>();
}
