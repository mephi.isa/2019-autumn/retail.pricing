package ru.chenko.Utils;


import com.google.gson.Gson;
import spark.ResponseTransformer;

public class JsonUtil {

    public static String toJson(Object object) {
        return new Gson().toJson(object);
    }

    public static Object fromJson(String json) {
        return new Gson().fromJson(json, Object.class);
    }

    public static ResponseTransformer json() {
        return JsonUtil::toJson;
    }
}
