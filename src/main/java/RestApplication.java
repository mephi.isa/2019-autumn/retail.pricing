import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import org.eclipse.jetty.io.ssl.ALPNProcessor;
import ru.chenko.PersonnelDao.PersonnelDao;
import ru.chenko.PersonnelDao.PersonnelDaoStub;
import ru.chenko.controllers.ServerController;
import ru.chenko.dao.Dao;
import ru.chenko.dao.DaoPostgres;
import ru.chenko.dao.DaoStub;
import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;
import org.apache.log4j.BasicConfigurator;
import freemarker.template.Template;
import javax.net.ssl.SSLEngine;

import java.util.*;

import static spark.Spark.*;

public class RestApplication {

    private static ALPNProcessor BasicConfigurator;
    private static Object Hashing;

    public static void main(String[] args) {
        startServer();
    }

    public static void startServer() {
        Dao dao = new DaoPostgres();
        PersonnelDao personnelDao = new PersonnelDaoStub();
        ServerController serverController = new ServerController(dao,personnelDao);
        serverController.run();
    }
}

