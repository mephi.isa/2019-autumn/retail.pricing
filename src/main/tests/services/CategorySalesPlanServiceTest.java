package services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.chenko.dao.Dao;
import ru.chenko.dao.DaoStub;
import ru.chenko.exceptions.*;
import ru.chenko.models.Quarter;
import ru.chenko.models.Status;
import ru.chenko.models.User;
import ru.chenko.services.Service;
import ru.chenko.services.ServiceImpl;

import java.io.IOException;
import java.sql.SQLException;

public class CategorySalesPlanServiceTest {
    Dao dao = new DaoStub();
    User user = new User();
    private Service service;
    private String name = "Bakery";

    @Before
    public void init(){
        service = new ServiceImpl(dao,user);
    }

    /*
    @Test
    public void TestCategorySalesPlanComplete()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalQuarterException, IllegalYearException, SQLException, ClassNotFoundException, IllegalSalesPlanStateException {
        Quarter quarter = new Quarter(4,2019);
        service.addCategorySalesPlan(quarter,name);
        service.CategorySalesPlanComplete(name,quarter);
        Assert.assertEquals(service.getCategoriesSalesPlan(name,quarter).status, Status.COMPLETED);
    }

    @Test
    public void TestCategorySalesPlanAccept()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalQuarterException, IllegalYearException, SQLException, ClassNotFoundException, IllegalSalesPlanStateException {
        Quarter quarter = new Quarter(4,2019);
        service.addCategorySalesPlan(quarter,name);
        service.CategorySalesPlanAccept(name,quarter);
        Assert.assertEquals(service.getCategoriesSalesPlan(name,quarter).status, Status.ACCEPTED);
    }

    @Test
    public void TestCategorySalesPlanReject()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalSalesPlanStateException, IllegalQuarterException, IllegalYearException, SQLException, ClassNotFoundException {
        Quarter quarter = new Quarter(4,2019);
        service.addCategorySalesPlan(quarter,name);
        service.CategorySalesPlanReject(name,quarter);
        Assert.assertEquals(service.getCategoriesSalesPlan(name,quarter).status, Status.REJECTED);
    }

    @Test
    public void TestCategorySalesPlanAddProduct()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalQuarterException, IllegalYearException, SQLException, ClassNotFoundException {
        Quarter quarter = new Quarter(4,2019);
        service.addCategorySalesPlan(quarter,name);
        service.CategorySalesPlanAddProduct(name,quarter,"123");
        Assert.assertEquals(service.getCategoriesSalesPlan(name,quarter).salesPlanRowList.iterator().next().getProductArticle(),"123");
    }

    @Test
    public void TestCategorySalesPlanRemoveProduct()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalQuarterException, IllegalYearException, SQLException, ClassNotFoundException {
        Quarter quarter = new Quarter(4,2019);
        service.addCategorySalesPlan(quarter,name);
        service.CategorySalesPlanAddProduct(name,quarter,"123");
    }*/




}
