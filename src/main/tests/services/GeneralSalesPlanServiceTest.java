package services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.chenko.dao.Dao;
import ru.chenko.dao.DaoStub;
import ru.chenko.exceptions.*;
import ru.chenko.models.Quarter;
import ru.chenko.models.Status;
import ru.chenko.services.Role;
import ru.chenko.services.Service;
import ru.chenko.services.ServiceImpl;
import ru.chenko.models.Product;
import ru.chenko.models.User;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.sql.SQLException;

public class GeneralSalesPlanServiceTest {
    Dao dao = new DaoStub();
    User user = new User();
    private Service service;
    private String name = "Milk";
    private String article = "12345";

    @Before
    public void init(){
        user.role = Role.SUPERVISOR;
        user.login = 123L;
        service = new ServiceImpl(dao,user);
    }

    @Test
    public void TestAddGeneralSalesPlan()
            throws IOException, InvalidSessionException, InvalidDataException, IllegalQuarterException, IllegalYearException, ClassNotFoundException, SQLException, EntitySavingException, IllegalCountException {
        Quarter quarter = new Quarter(4,2020);
        service.addGeneralSalesPlan(quarter);
        Assert.assertEquals(service.getGeneralSalesPlanList().size(),1);
    }

    @Test
    public void TestGeneralSalesPlanAccept()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalQuarterException, IllegalYearException, ClassNotFoundException, SQLException, IllegalCountException, IllegalSalesPlanStateException {
        Quarter quarter = new Quarter(4,2020);
        service.addGeneralSalesPlan(quarter);
        service.GeneralSalesPlanAccept(quarter);
        Assert.assertEquals(service.getGeneralSalesPlanByQuarter(quarter).status, Status.ACCEPTED);
    }

    @Test
    public void TestGeneralSalesPlanComplete()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalSalesPlanStateException, IllegalQuarterException, IllegalYearException, SQLException, ClassNotFoundException, IllegalCountException {
        Quarter quarter = new Quarter(4,2020);
        service.addCategory(name,name);
        service.addProduct(article,name,name);
        service.addGeneralSalesPlan(quarter);
        service.CategorySalesPlanSetProductQuantity(name,quarter,article,150);
        service.CategorySalesPlanComplete(name,quarter);
        Assert.assertEquals(service.getGeneralSalesPlanByQuarter(quarter).status, Status.COMPLETED);
    }

    @Test
    public void TestGeneralSalesPlanReject()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, IllegalSalesPlanStateException, IllegalQuarterException, IllegalYearException, SQLException, ClassNotFoundException, IllegalCountException {
        Quarter quarter = new Quarter(4,2020);
        service.addCategory(name,name);
        service.addProduct(article,name,name);
        service.addGeneralSalesPlan(quarter);
        service.CategorySalesPlanSetProductQuantity(name,quarter,article,150);
        service.CategorySalesPlanComplete(name,quarter);
        service.GeneralSalesPlanReject(quarter);
        Assert.assertEquals(Status.REJECTED,service.getCategoriesSalesPlan(name,quarter).status);
        Assert.assertEquals(Status.REJECTED,service.getGeneralSalesPlanByQuarter(quarter).status);
    }
}
