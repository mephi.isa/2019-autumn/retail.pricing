package services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.chenko.dao.Dao;
import ru.chenko.dao.DaoStub;
import ru.chenko.exceptions.*;
import ru.chenko.models.Status;
import ru.chenko.services.Service;
import ru.chenko.services.ServiceImpl;
import ru.chenko.models.Product;
import ru.chenko.models.User;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.sql.SQLException;

public class CategoryServiceTest {
    Dao dao = new DaoStub();
    User user = new User();
    private Service service;
    private String name = "Bakery";
    private String description = "Include bread, rolls, cookies, pies, pastries, and muffins";

    @Before
    public void init(){
        service = new ServiceImpl(dao,user);
    }

    @Test
    public void TestAddCategory()
            throws IOException, InvalidSessionException, InvalidDataException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        service.addCategory(name,description);
        Assert.assertEquals(service.getCategory(name).description,description);
    }

    @Test
    public void TestAddAnalyst()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        service.addCategory(name,description);
        service.addAnalyst(name,123L);
        Assert.assertEquals(service.getCategory(name).analystList.size(),1L);
    }

    @Test
    public void TestRemoveAnalyst()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        service.addCategory(name,description);
        service.removeAnalyst(name,123L);
        service.addAnalyst(name,123L);
        Assert.assertEquals(service.getCategory(name).analystList.size(),0L);
    }

    @Test
    public void TestRemoveProduct()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        service.addCategory(name,description);
        service.addProduct("123","Bylochka",name);
        service.removeProductFromCategory(name,"123");
        Assert.assertEquals(service.getCategory(name).productArticlesList,0L);
    }

    @Test
    public void TestSetManager()
            throws IOException, InvalidSessionException, InvalidDataException, EntitySavingException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        service.addCategory(name,description);
        service.setManager(name,123L);
        Assert.assertEquals(java.util.Optional.ofNullable(service.getCategory(name).manager),123L);
    }

}
