package models;

import ru.chenko.exceptions.IllegalQuarterException;
import ru.chenko.exceptions.IllegalSalesPlanStateException;
import ru.chenko.exceptions.IllegalYearException;
import ru.chenko.models.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GeneralSalesPlanTest {

    private GeneralSalesPlan generalSalesPlan;

    private Quarter quarter;
    private Category category;


    @Before
    public void init() throws IllegalYearException, IllegalQuarterException {
        this.quarter = new Quarter(4, 2019);
        this.category = new Category("Bakery", "Include bread, rolls, cookies, pies, pastries, and muffins");
        this.generalSalesPlan = new GeneralSalesPlan(quarter);
    }

    @Test
    public void TestPositive_GeneralSalesPlanConstructor() {
        GeneralSalesPlan generalSalesPlan = new GeneralSalesPlan(quarter);
        Assert.assertTrue(generalSalesPlan.getQuarter().equals(quarter) && generalSalesPlan.getStatus() == Status.CREATED);
    }

    @Test
    public void TestPositive_CategoryTestAddProduct() {
        CategorySalesPlan categorySalesPlan = new CategorySalesPlan(quarter, category.getName());
        generalSalesPlan.addCategorySalesPlan(category.getName());

        Assert.assertTrue(generalSalesPlan.getCategorySalesPlanSet().contains(categorySalesPlan));
    }

    @Test
    public void TestPositive_CategoryTestAddProductDuplicate() {
        CategorySalesPlan categorySalesPlan = new CategorySalesPlan(quarter, category.getName());
        generalSalesPlan.addCategorySalesPlan(category.getName());
        generalSalesPlan.addCategorySalesPlan(category.getName());

        int expectingCount = 1;
        Assert.assertEquals(expectingCount, generalSalesPlan.getCategorySalesPlanSet().stream().filter(el -> el.equals(categorySalesPlan)).count());
    }

    @Test
    public void TestPositive_CategoryTestRemoveProduct() {
        CategorySalesPlan categorySalesPlan = new CategorySalesPlan(quarter, category.getName());
        generalSalesPlan.addCategorySalesPlan(category.getName());

        generalSalesPlan.removeCategorySalesPlan(category.getName());
        Assert.assertFalse(generalSalesPlan.getCategorySalesPlanSet().contains(categorySalesPlan));
    }

    @Test
    public void TestPositive_CategoryTestRemoveProductNonexistent() {
        CategorySalesPlan categorySalesPlan = new CategorySalesPlan(quarter, category.getName());

        generalSalesPlan.removeCategorySalesPlan(category.getName());
        Assert.assertFalse(generalSalesPlan.getCategorySalesPlanSet().contains(categorySalesPlan));
    }

    @Test
    public void TestPositive_CategoryTestGetProductList() {
        CategorySalesPlan firstSalesPlan = new CategorySalesPlan(quarter, category.getName());
        CategorySalesPlan secondSalesPlan = new CategorySalesPlan(quarter, category.getName());

        generalSalesPlan.addCategorySalesPlan(category.getName());
        generalSalesPlan.addCategorySalesPlan(category.getName());

        Assert.assertEquals(generalSalesPlan.getCategorySalesPlanSet().size(), 1);
    }
}
