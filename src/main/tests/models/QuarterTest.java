package models;

import ru.chenko.exceptions.IllegalQuarterException;
import ru.chenko.exceptions.IllegalYearException;
import ru.chenko.models.Quarter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class QuarterTest {


    @Test
    public void TestPositive_QuarterConstructor() throws IllegalQuarterException, IllegalYearException {
        Integer expectingQuarter = 2;
        Integer expectingYear = 2019;
        Quarter quarter = new Quarter(expectingQuarter, expectingYear);

        Assert.assertTrue(quarter.getQuarter().equals(expectingQuarter) && quarter.getYear().equals(expectingYear));
    }

    @Test(expected = IllegalQuarterException.class)
    public void TestIllegalQuarterException_QuarterConstructor() throws IllegalQuarterException, IllegalYearException {
        Integer expectingQuarter = 6;
        Integer expectingYear = 2019;
        Quarter quarter = new Quarter(expectingQuarter, expectingYear);
    }

    @Test(expected = IllegalYearException.class)
    public void TestIllegalYearException_QuarterConstructor() throws IllegalQuarterException, IllegalYearException {
        Integer expectingQuarter = 2;
        Integer expectingYear = 1042;
        Quarter quarter = new Quarter(expectingQuarter, expectingYear);
    }
}
