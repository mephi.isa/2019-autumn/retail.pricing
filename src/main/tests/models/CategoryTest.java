package models;

import ru.chenko.Dto.CategoryDto;
import ru.chenko.models.Category;
import ru.chenko.models.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CategoryTest {

    private String name = "Bakery";
    private String description = "Include bread, rolls, cookies, pies, pastries, and muffins";
    private Category category;
    private CategoryDto categoryDto;

    @Before
    public void init() {
        category = new Category(name, description);
        categoryDto = new CategoryDto();
    }

    @Test
    public void TestPositive_CategoryConstructor() {
        String expectingName = name;
        String expectingDescription = description;
        Category category = new Category(expectingName, expectingDescription);

        Assert.assertTrue(category.getName().equals(expectingName) && category.getDescription().equals(expectingDescription));
    }

    @Test
    public void TestPositive_CategorySetManager() {
        Long expectingManagerId = 57L;

        this.category.setManager(expectingManagerId);
        Assert.assertEquals(category.getManager(), expectingManagerId);
    }

    @Test
    public void TestPositive_CategoryAddAnalyst() {
        Long analystId = 17L;

        category.addAnalyst(analystId);
        Assert.assertTrue(category.getAnalystSet().contains(analystId));
    }

    @Test
    public void TestPositive_CategoryAddAnalystDuplication() {
        Long analystId = 23L;
        category.addAnalyst(analystId);
        category.addAnalyst(analystId);

        int expectedCount = 1;
        Assert.assertEquals(expectedCount, category.getAnalystSet().stream().filter(el -> el.floatValue() == analystId).count());
    }

    @Test
    public void TestPositive_CategoryRemoveAnalyst() {
        Long analystId = 15L;
        category.addAnalyst(analystId);

        category.removeAnalyst(analystId);
        Assert.assertFalse(category.getAnalystSet().contains(analystId));
    }

    @Test
    public void TestPositive_CategoryRemoveAnalystNotexistent() {
        Long analystId = 15L;

        category.removeAnalyst(analystId);
        Assert.assertFalse(category.getAnalystSet().contains(analystId));
    }

    @Test
    public void TestPositive_CategoryGetAnalystList() {
        category.addAnalyst(12L);
        category.addAnalyst(15L);
        category.addAnalyst(28L);

        Assert.assertEquals(category.getAnalystSet().size(), 3);
    }

    @Test
    public void TestPositive_CategoryAddProduct() {
        Product product = new Product("1","Bylochka", categoryDto.name);
        category.addProduct(product.getArticle());

        Assert.assertTrue(category.getProductSet().contains(product));
    }

    @Test
    public void TestPositive_CategoryAddProductDuplicate() {
        Product product = new Product("1","Bylochka", categoryDto.name);
        category.addProduct(product.getArticle());
        category.addProduct(product.getArticle());

        int expectedCount = 1;
        Assert.assertEquals(expectedCount, category.getProductSet().stream().filter(el -> el.equals(product)).count());
    }

    @Test
    public void TestPositive_CategoryRemoveProduct() {
        Product product = new Product("1","Bylochka", categoryDto.name);
        category.addProduct(product.getArticle());

        category.removeProduct(product);
        Assert.assertFalse(category.getProductSet().contains(product));
    }

    @Test
    public void TestPositive_CategoryRemoveProductNonexistent() {
        Product product = new Product("1","Bylochka", categoryDto.name);

        category.removeProduct(product);
        Assert.assertFalse(category.getProductSet().contains(product));
    }

    @Test
    public void TestPositive_CategoryGetProductList() {
        Product firstProduct = new Product("1","Bylochka", categoryDto.name);
        Product secondProduct = new Product("2","Bylochka", categoryDto.name);

        category.addProduct(firstProduct.getArticle());
        category.addProduct(secondProduct.getArticle());

        Assert.assertEquals(category.getProductSet().size(), 2);
    }
}
