package models;

import ru.chenko.Dto.CategoryDto;
import ru.chenko.exceptions.IllegalQuarterException;
import ru.chenko.exceptions.IllegalYearException;
import ru.chenko.models.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CategorySalesPlanTest {

    private CategorySalesPlan categorySalesPlan;

    private Category category;
    private CategoryDto categoryDto;


    @Before
    public void init() throws IllegalYearException, IllegalQuarterException {
        Quarter quarter = new Quarter(4, 2019);
        this.category = new Category("Bakery", "Include bread, rolls, cookies, pies, pastries, and muffins");
        this.categorySalesPlan = new CategorySalesPlan(quarter, category.getName());
        categoryDto = new CategoryDto();
    }

    @Test
    public void TestPositive_CategorySalesPlanConstructor() throws IllegalQuarterException, IllegalYearException {
        Quarter expectedQuarter = new Quarter(4, 2019);
        CategorySalesPlan categorySalesPlan = new CategorySalesPlan(expectedQuarter, category.getName());

        Assert.assertTrue(categorySalesPlan.getQuarter().equals(expectedQuarter) && categorySalesPlan.getCategory().equals(category.getName()) &&
                categorySalesPlan.getStatus().equals(Status.CREATED));
    }

    @Test
    public void TestPositive_CategorySalesPlanAddProduct() {
        Product product = new Product("1","Bylochka", categoryDto.name);
        categorySalesPlan.addProduct(product.getArticle());

        Assert.assertTrue(categorySalesPlan.getSalesPlanRowSet().size() == 1);
    }

    @Test
    public void TestPositive_CategorySalesPlanAddProductDuplicate() {
        Product product = new Product("1","Bylochka", categoryDto.name);
        categorySalesPlan.addProduct(product.getArticle());
        categorySalesPlan.addProduct(product.getArticle());

        int expectedCount = 1;
        Assert.assertEquals(expectedCount, categorySalesPlan.getSalesPlanRowSet().size());
    }

    @Test
    public void TestPositive_CategorySalesPlanRemoveProduct() {
        Product product = new Product("1","Bylochka", categoryDto.name);
        categorySalesPlan.addProduct(product.getArticle());

        categorySalesPlan.removeProduct(product.getArticle());
        Assert.assertNotEquals(1, categorySalesPlan.getSalesPlanRowSet().size());
    }

    @Test
    public void TestPositive_CategorySalesPlanRemoveProductNonexistent() {
        Product product = new Product("1","Bylochka", categoryDto.name);

        categorySalesPlan.removeProduct(product.getArticle());
        Assert.assertNotEquals(0, categorySalesPlan.getSalesPlanRowSet().size());
    }

    @Test
    public void TestPositive_CategorySalesPlanGetProductList() {
        Product firstProduct = new Product("1","Bylochka", categoryDto.name);
        Product secondProduct = new Product("2","Bylochka", categoryDto.name);
        Product thirdProduct = new Product("3","Bylochka", categoryDto.name);
        Product fourthProduct = new Product("4","Bylochka", categoryDto.name);


        categorySalesPlan.addProduct(firstProduct.getArticle());
        categorySalesPlan.addProduct(secondProduct.getArticle());
        categorySalesPlan.addProduct(thirdProduct.getArticle());
        categorySalesPlan.addProduct(fourthProduct.getArticle());

        Assert.assertEquals(categorySalesPlan.getSalesPlanRowSet().size(), 4);
    }
}
