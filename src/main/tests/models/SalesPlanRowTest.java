package models;

import ru.chenko.Dto.CategoryDto;
import ru.chenko.exceptions.IllegalCountException;
import ru.chenko.exceptions.IllegalQuarterException;
import ru.chenko.exceptions.IllegalYearException;
import ru.chenko.models.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SalesPlanRowTest {

    private SalesPlanRow salesPlanRow;

    @Before
    public void init() throws IllegalYearException, IllegalQuarterException {
        Quarter quarter = new Quarter(4, 2019);
        Category category = new Category("Bakery", "Include bread, rolls, cookies, pies, pastries, and muffins");
        CategorySalesPlan categorySalesPlan = new CategorySalesPlan(quarter,category.getName());
        CategoryDto categoryDto = new CategoryDto();
        Product product = new Product("1","Bylochka", categoryDto.name);
        this.salesPlanRow = new SalesPlanRow(product.getArticle());
    }

    @Test
    public void TestPositive_SalesPlanRowSetCount() throws IllegalCountException {
        Integer expectingCount = 6;

        salesPlanRow.setCount(expectingCount);
        Assert.assertEquals(salesPlanRow.getCount(), expectingCount);
    }

    @Test(expected = IllegalCountException.class)
    public void TestNegative_SalesPlanRowSetCount() throws IllegalCountException {
        Integer expectingCount = -2;

        salesPlanRow.setCount(expectingCount);
    }

    @Test
    public void TestZero_SalesPlanRowSetCount() throws IllegalCountException {
        Integer expectingCount = 0;

        salesPlanRow.setCount(expectingCount);
        Assert.assertEquals(salesPlanRow.getCount(), expectingCount);
    }
}
