package models;

import ru.chenko.Dto.CategoryDto;
import ru.chenko.exceptions.IllegalPriceException;
import ru.chenko.models.Category;
import ru.chenko.models.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProductTest {

    private String article = "1";
    private Float price = 0f;
    private Product product;
    private CategoryDto category;

    @Before
    public void init() {
        this.category = new CategoryDto();
        this.product = new Product(article,"Bylochka", category.name);
    }

    @Test
    public void TestPositive_ProductConstructor() {
        String expectingArticle = article;
        CategoryDto expectingCategory = category;
        Product product = new Product(expectingArticle,"Bylochka", expectingCategory.name);

        Assert.assertTrue(product.getArticle().equals(expectingArticle) && product.getCategoryName().equals(expectingCategory.name));
    }


}
