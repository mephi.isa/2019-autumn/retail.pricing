package dtl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.chenko.Dto.CategoryDto;
import ru.chenko.dao.Dao;
import ru.chenko.dao.DaoPostgres;
import ru.chenko.dao.DaoPostgresV2;
import ru.chenko.exceptions.*;
import ru.chenko.models.Quarter;
import ru.chenko.models.Status;
import ru.chenko.models.User;
import ru.chenko.services.CategorySalesPlanService;
import ru.chenko.services.Role;
import ru.chenko.services.Service;
import ru.chenko.services.ServiceImpl;

import java.io.IOException;
import java.sql.SQLException;

public class DaoPostgresTest {
    Dao dao = new DaoPostgresV2();
    User user = new User();
    private Service service;
    private CategorySalesPlanService categorySalesPlanService;

    private String name = "Milk";
    private String description = "Include bread, rolls, cookies, pies, pastries";

    public DaoPostgresTest() throws SQLException {
    }

    @Before
    public void init(){
        user.login = 124L;
        user.role = Role.SUPERVISOR;
        service = new ServiceImpl(dao,user);
        categorySalesPlanService = new CategorySalesPlanService(dao,user);
    }

    @Test
    public void TestAddCategory()
            throws IOException, InvalidSessionException, InvalidDataException, SQLException, ClassNotFoundException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        service.addCategory(name,description);
    }

    @Test
    public void TestGetCategory()
            throws IOException, InvalidSessionException, SQLException, ClassNotFoundException {
        CategoryDto categoryDto = service.getCategory(name);
        Assert.assertEquals(categoryDto.description, description);
    }

    @Test
    public void updateCategory()
            throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, InvalidDataException, EntitySavingException {
        service.setManager(name,1L);
    }

    @Test
    public void updateCategoryAddAnalyst()
            throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, InvalidDataException, EntitySavingException {
        service.addAnalyst(name,1L);
    }

    @Test
    public void addProduct()
            throws IOException, SQLException, ClassNotFoundException, InvalidDataException, EntitySavingException, IllegalCountException, IllegalQuarterException, IllegalYearException {
        service.addProduct("56789","Искренне ваш",name);
    }

    @Test
    public void addCategorySalesPlan()
            throws IOException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, InvalidDataException, IllegalCountException {
        Quarter quarter = new Quarter(1,2020);
        categorySalesPlanService.addCategorySalesPlan(quarter,name);
    }

    @Test
    public void getCategorySalesPlan()
            throws IOException, InvalidSessionException, SQLException, ClassNotFoundException,
            IllegalQuarterException, IllegalYearException, IllegalCountException {
        Quarter quarter = new Quarter(1,2020);
        Assert.assertEquals(service.getCategoriesSalesPlan("Milk",quarter).status, Status.COMPLETED);
    }

    @Test
    public void updateCategorySalesPlan() throws InvalidSessionException, IllegalSalesPlanStateException, IOException, SQLException, InvalidDataException, EntitySavingException, ClassNotFoundException, IllegalQuarterException, IllegalYearException, IllegalCountException {
        Quarter quarter = new Quarter(1,2020);
        categorySalesPlanService.CategorySalesPlanSetCount(name,quarter,"12345",15325);
        categorySalesPlanService.CategorySalesPlanSetCount(name,quarter,"54321",1488);
        service.CategorySalesPlanComplete("Milk",quarter);
        Assert.assertEquals(service.getCategoriesSalesPlan("Milk",quarter).status, Status.COMPLETED);
    }

    @Test
    public void addGeneralSalesPlan()
            throws SQLException, InvalidDataException, InvalidSessionException, IOException,
            IllegalQuarterException, IllegalYearException, EntitySavingException, ClassNotFoundException, IllegalCountException {
        Quarter quarter = new Quarter(1,2020);
        service.addGeneralSalesPlan(quarter);
    }

    @Test
    public void getGeneralSalesPlan()
            throws IOException, InvalidSessionException, SQLException, ClassNotFoundException, IllegalQuarterException, IllegalYearException {
        Quarter quarter = new Quarter(1,2020);
        Assert.assertEquals(service.getGeneralSalesPlanByQuarter(quarter).status, Status.COMPLETED);
    }




}
